<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

// print_r($_POST);

if (isset($_POST['tecnicoselect']) &&
    isset($_POST['password']))   {

    require("db_connection.php");
    require("use_db.php");

    $idtecnico = (int) $_POST['tecnicoselect'];
    $password = $_POST['password'];

    if ($password === "root") {


        $stmt = $conn->prepare("UPDATE tecnici
                                SET Attivo = 0
                                WHERE IdTecnico = ?"); // non opera a cascata


        $stmt->bind_param("i", $idtecnico);

        if ($stmt->execute() == TRUE) {
            echo "<script>
            alert('Tecnico eliminato con successo!');
            window.location.href='logout.php';
            </script>";

        } else {
          echo $stmt->error;
        }

        $stmt->close();

      
      $conn->close();
    } else {
      echo "<script>
      alert('Password amministratore errata. Tecnico non eliminato.');
      window.location.href='home.php';
      </script>";

    }

}
?>
