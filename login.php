<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Login</title>
</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="login-header">
            <figure>
              <img class="logodesktop" src="repairitlogo.png" alt="Repair It Logo">
              <figcaption></figcaption>
            </figure>
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
        <div class="row login">
          <div class="offset-sm-2 col-sm-8">
            <form class="form-signin" action="login_script.php" method="post">
                <p class="form-signin-heading">Inserisci le credenziali</p>
                <label for="idtecnico" class="sr-only">Id Tecnico</label>
                <input type="number" id="idtecnico" name="idtecnico" class="form-control" placeholder="Id Tecnico" autocomplete="on" min="0" required autofocus>
                <label for="password" class="sr-only">Password</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                <button class="btn btn-lg btn-secondary btn-block" type="submit"><span class="glyphicon glyphicon-log-in"></span> Entra</button>
            </form>
          </div>
        </div>
        <div class="row registration">
          <div class="offset-sm-2 col-sm-8">
            <a id="registrationlink" href="registrazione.php">Non sei registrato? Clicca qui.</a>
          </div>
        </div>
        <div class="row footer">
          <div class="col-sm-12">
            <footer>
              Sito sviluppato da Pivato Francesco
            </footer>
          </div>
        </div>
    </div>
</body>
</html>
