<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['idtecnico']) && isset($_POST['password']))   {

    require("db_connection.php");

    $idtecnico = $_POST['idtecnico'];
    $password = $_POST['password'];

    require("use_db.php");

    $stmt = $conn->prepare("SELECT * FROM tecnici WHERE (IdTecnico=?) AND (Password=?) AND Attivo=1");

    $stmt->bind_param("ss", $idtecnico, $password);

    $stmt->execute();

    $result = $stmt->get_result();

    if ($result->num_rows == 1) {

        $row = $result->fetch_assoc();
        $_SESSION['IdTecnico'] = $row["IdTecnico"];
        $_SESSION['Nome'] = $row["Nome"];
        $_SESSION['Cognome'] = $row["Cognome"];
        $_SESSION['Email'] = $row["Email"];
        $_SESSION['Telefono'] = $row["Telefono"];

        $_SESSION['account'] = $_POST['idtecnico'];

        //login riuscito
        $_SESSION['result'] = true;

        echo "<script>
        alert('Benvenuto, " . $_SESSION['Nome'] . " " . $_SESSION['Cognome'] ."!');
        window.location.href='home.php';
        </script>";

    } else {

      if ($idtecnico == 0 && $password == "root") {
        echo "<script>
        alert('Benvenuto, Amministratore!');
        window.location.href='homeadmin.php';
        </script>";
      } else {
        // credenziali errate
        $_SESSION['result'] = false;
        echo "<script>
        alert('Id tecnico o password errati!');
        window.location.href='login.php';
        </script>";
      }

    }

    $result->close();
    $stmt->close();
    $conn->close();

}

?>
