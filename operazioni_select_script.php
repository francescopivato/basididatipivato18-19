<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  // print_r($_POST);

  if (isset($_POST['idC']) &&
      isset($_POST['idD']) &&
      isset($_POST['idI'])) {



    require("db_connection.php");
    require("use_db.php");

    $idCliente = (int)($_POST['idC']);
    $idDispositivo = (int)($_POST['idD']);
    $idIntervento = (int)($_POST['idI']);

    $selectoperazioni = $conn->prepare("SELECT
                                            O.IdOperazione,
                                            O.DataInizio,
                                            O.DataFine,
                                            O.Commento,
                                            O.Tecnico,
                                            T.Nome,
                                            T.Cognome
                                        FROM
                                            operazioni O
                                        INNER JOIN tecnici T
                                        	ON O.Tecnico = T.IdTecnico
                                        WHERE
                                            Cliente = ? AND Dispositivo = ? AND Intervento = ?");

    $selectoperazioni->bind_param("iii", $idCliente, $idDispositivo, $idIntervento);
    $selectoperazioni->execute();
    $result = $selectoperazioni->get_result();

    echo "<table class=\"table\">
    <thead>
    <tr>
    <th scope=\"col\">Id Operazione</th>
    <th scope=\"col\">Data Inizio</th>
    <th scope=\"col\">Data Fine</th>
    <th scope=\"col\">Commento</th>
    <th scope=\"col\">Id Tecnico</th>
    <th scope=\"col\">Nome</th>
    <th scope=\"col\">Cognome</th>
    </tr>
    </thead>
    <tbody>";

    while ($row = $result->fetch_assoc()) {
      echo "<tr>";
      // echo "<th scope=\"row\">";
      echo "<td>" . $row['IdOperazione'] . "</td>";
      echo "<td>" . $row['DataInizio'] . "</td>";
      echo "<td>" . $row['DataFine'] . "</td>";
      echo "<td>" . $row['Commento'] . "</td>";
      echo "<td>" . $row['Tecnico'] . "</td>";
      echo "<td>" . $row['Nome'] . "</td>";
      echo "<td>" . $row['Cognome'] . "</td>";
      // echo "</th>";
      echo "</tr>";
     }

     echo "</tbody></table>";

    $selectoperazioni->close();

  }

?>
