<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Interventi</title>

    <script type="text/javascript">

    function mostraInserisci(){
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#inserisci").toggle();
    }

    function mostraModifica(){
      $("#inserisci").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#modifica").toggle();
    }

    function mostraElimina(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#visualizza").hide();
      $("#elimina").toggle();
    }

    function mostraVisualizza(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").toggle();
    }

    function getDispositivi(idCliente){ // per inserire intervento

      $.post("dispositivi_select_script.php", {
        id: idCliente
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#dispositivoselect").children().remove();
        $("#dispositivoselect").append(data);
      });

    }

    function getDispositiviTermina(idCliente){ // per terminare intervento

      $.post("dispositivi_select_script.php", {
        id: idCliente
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#dispositivoselecttermina").children().remove();
        $("#dispositivoselecttermina").append(data);
      });

    }


    function getInterventi(idCliente){

      $.post("interventi_visualizza_script.php", {
        id: idCliente
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#txtInterventi").children().remove();
        $("#txtInterventi").append(data);
      });

    }

    </script>

</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="home.php">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="home.php">Home</a>
                <a class="nav-item nav-link" href="clienti.php">Clienti</a>
                <a class="nav-item nav-link" href="dispositivi.php">Dispositivi</a>
                <a class="nav-item nav-link" href="#">Interventi</a>
                <a class="nav-item nav-link" href="operazioni.php">Operazioni</a>
                <a class="nav-item nav-link" href="pagamenti.php">Pagamenti</a>
                <a class="nav-item nav-link" href="tecnici.php">Tecnici</a>
                <a class="nav-item nav-link" href="acquisti.php">Acquisti</a>
                <a class="nav-item nav-link" href="categorie.php">Categorie</a>
                <a class="nav-item nav-link" href="elementi.php">Elementi</a>
                <a class="nav-item nav-link" href="utilizzi.php">Utilizzi</a>
                <a class="nav-item nav-link" href="logout.php">Logout</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row buttons">
              <div class="offset-sm-2 col-sm-8">
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraInserisci()">Inserisci i dati di un intervento</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraModifica()">Termina un intervento</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraElimina()">Visualizza gli interventi in corso</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraVisualizza()">Visualizza tutti gli interventi di un cliente</button>
              </div>
            </div>
            <div class="row actions">
              <div class="offset-sm-1 col-sm-10">
                <div class="inserisci initiallyhidden" id="inserisci">
                  <form action="interventi_inserisci_script.php" method="post">
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona il cliente: </legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="clienteselect" class="col-sm-2 col-form-label sr-only">Cliente: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="clienteselect" placeholder="Cliente" size="8" onchange="getDispositivi(this.value)" required >
                            <?php

                              require("db_connection.php");
                              require("use_db.php");

                              $selectclienti = $conn->prepare("SELECT IdCliente, Cognome, Nome, Email FROM clienti");
                              $selectclienti->execute();
                              $selectclienti->bind_result($id, $cognome, $nome, $email);

                              while ($selectclienti->fetch()) {
                                echo "<option value=\"" . $id . "\">" . $id . " " . $cognome . " " . $nome . " " . $email . "</option>";
                              }

                              $selectclienti->close();

                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona il dispositivo</legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="dispositivoselect" class="col-sm-2 col-form-label sr-only">Dispositivo: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="dispositivoselect" id="dispositivoselect" placeholder="Dispositivo" size="8" required >
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                        <div class="row legend">
                          <div class="offset-sm-2 col-sm-8">
                            <legend>Dati Intervento</legend>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="deposito" class="col-sm-2 col-form-label sr-only">Deposito: </label>
                          <div class="offset-sm-2 col-sm-8">
                            <input type="number" id="deposito" name="deposito" class="form-control" placeholder="Deposito" step="0.01" min="0" required>
                          </div>
                        </div>
                      </fieldset>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-8">
                        <input class="btn btn-lg btn-secondary" type="submit" name="inserisci" id="inserisci" value="Inserisci">
                        <input class="btn btn-lg btn-secondary" type="reset" id="reset" value="Reset">
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modifica initiallyhidden" id="modifica">
                  <form action="interventi_termina_script.php" method="post">
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona il cliente: </legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="clienteselecttermina" class="col-sm-2 col-form-label sr-only">Cliente: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="clienteselecttermina" placeholder="Cliente" size="8" onchange="getDispositiviTermina(this.value)" required >
                            <?php

                              require("db_connection.php");
                              require("use_db.php");

                              $selectclienti = $conn->prepare("SELECT IdCliente, Cognome, Nome, Email FROM clienti");
                              $selectclienti->execute();
                              $selectclienti->bind_result($id, $cognome, $nome, $email);

                              while ($selectclienti->fetch()) {
                                echo "<option value=\"" . $id . "\">" . $id . " " . $cognome . " " . $nome . " " . $email . "</option>";
                              }

                              $selectclienti->close();

                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona il dispositivo</legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="dispositivoselecttermina" class="col-sm-2 col-form-label sr-only">Dispositivo: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="dispositivoselecttermina" id="dispositivoselecttermina" placeholder="Dispositivo" size="8" required >
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-8">
                        <input class="btn btn-lg btn-secondary" type="submit" name="termina" id="termina" value="Termina">
                        <input class="btn btn-lg btn-secondary" type="reset" id="reset" value="Reset">
                      </div>
                    </div>
                  </form>
                </div>
                <div class="elimina initiallyhidden" id="elimina">
                  <?php

                  require("db_connection.php");
                  require("use_db.php");

                  $selectpagamenti = $conn->prepare("SELECT
                                                          C.IdCliente,
                                                          C.Cognome,
                                                          C.Nome,
                                                          D.IdDispositivo,
                                                          D.NumSeriale,
                                                          D.Marca,
                                                          D.Modello,
                                                          I.IdIntervento,
                                                          I.DataRicezione
                                                      FROM
                                                          interventi I
                                                      INNER JOIN dispositivi D ON
                                                          I.Dispositivo = D.IdDispositivo AND I.Cliente = D.Cliente
                                                      INNER JOIN clienti C ON
                                                          I.Cliente = C.IdCliente
                                                      WHERE
                                                          I.DataFine IS NULL
                                                      ORDER BY I.DataRicezione");
                  $selectpagamenti->execute();

                  $result = $selectpagamenti->get_result();

                  echo "<table class=\"table\">
                  <thead>
                  <tr>
                  <th scope=\"col\">IdCliente</th>
                  <th scope=\"col\">Cognome</th>
                  <th scope=\"col\">Nome</th>
                  <th scope=\"col\">IdDispositivo</th>
                  <th scope=\"col\">Seriale</th>
                  <th scope=\"col\">Marca</th>
                  <th scope=\"col\">Modello</th>
                  <th scope=\"col\">IdIntervento</th>
                  <th scope=\"col\">DataRicezione</th>
                  </tr>
                  </thead>
                  <tbody>";

                  while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    // echo "<th scope=\"row\">";
                    echo "<td>" . $row['IdCliente'] . "</td>";
                    echo "<td>" . $row['Cognome'] . "</td>";
                    echo "<td>" . $row['Nome'] . "</td>";
                    echo "<td>" . $row['IdDispositivo'] . "</td>";
                    echo "<td>" . $row['NumSeriale'] . "</td>";
                    echo "<td>" . $row['Marca'] . "</td>";
                    echo "<td>" . $row['Modello'] . "</td>";
                    echo "<td>" . $row['IdIntervento'] . "</td>";
                    echo "<td>" . $row['DataRicezione'] . "</td>";
                    // echo "</th>";
                    echo "</tr>";
                   }

                   echo "</tbody></table>";


                    $selectpagamenti->close();

                  // }
                  $conn->close();


                   ?>
                </div>
                <div class="visualizza initiallyhidden mt-3" id="visualizza">
                  <form action="#" method="post">
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona il cliente: </legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="clientevisualizza" class="col-sm-2 col-form-label sr-only">Cliente: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="clientevisualizza" placeholder="Cliente" size="8" onchange="getInterventi(this.value)" required >
                            <?php

                              require("db_connection.php");
                              require("use_db.php");

                              $selectclienti = $conn->prepare("SELECT IdCliente, Cognome, Nome, Email FROM clienti");
                              $selectclienti->execute();
                              $selectclienti->bind_result($id, $cognome, $nome, $email);

                              while ($selectclienti->fetch()) {
                                echo "<option value=\"" . $id . "\">" . $id . " " . $cognome . " " . $nome . " " . $email . "</option>";
                              }

                              $selectclienti->close();

                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                  <div id="txtInterventi" class="col-sm-12"></div>
                </div>
              </div>
              </div>
          </main>
        </div>
      </div>
    </div>
</body>
</html>
