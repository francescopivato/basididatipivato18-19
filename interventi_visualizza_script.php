<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['id']) )   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['id'];

    $selectinterventi = $conn->prepare("SELECT
                                              DI.IdDispositivo,
                                              DI.Marca,
                                              DI.Modello,
                                              DI.NumSeriale,
                                              I.IdIntervento,
                                              I.DataRicezione,
                                              I.DataFine,
                                              I.DataRitorno,
                                              DE.Importo AS Deposito,
                                              P.Importo AS Pagamento,
                                              I.CostoTotale
                                          FROM
                                              interventi I
                                          INNER JOIN dispositivi DI ON
                                              I.Dispositivo = DI.IdDispositivo AND I.Cliente = DI.Cliente
                                          INNER JOIN depositi DE ON
                                              I.Deposito = De.IdDeposito
                                          LEFT OUTER JOIN pagamenti P ON
                                              I.Pagamento = P.IdPagamento
                                          WHERE
                                              I.Cliente = ?");
    $selectinterventi->bind_param("i", $idcliente);
    $selectinterventi->execute();
    $result = $selectinterventi->get_result();

    echo "<table class=\"table\">
    <thead>
    <tr>
    <th scope=\"col\">IdDispositivo</th>
    <th scope=\"col\">Marca</th>
    <th scope=\"col\">Modello</th>
    <th scope=\"col\">Seriale</th>
    <th scope=\"col\">IdIntervento</th>
    <th scope=\"col\">Data Ricezione</th>
    <th scope=\"col\">Data Fine</th>
    <th scope=\"col\">Data Ritorno</th>
    <th scope=\"col\">Deposito</th>
    <th scope=\"col\">Pagamento</th>
    <th scope=\"col\">Costo Totale</th>
    </tr>
    </thead>
    <tbody>";

    while ($row = $result->fetch_assoc()) {
      echo "<tr>";
      // echo "<th scope=\"row\">";
      echo "<td>" . $row['IdDispositivo'] . "</td>";
      echo "<td>" . $row['Marca'] . "</td>";
      echo "<td>" . $row['Modello'] . "</td>";
      echo "<td>" . $row['NumSeriale'] . "</td>";
      echo "<td>" . $row['IdIntervento'] . "</td>";
      echo "<td>" . $row['DataRicezione'] . "</td>";
      echo "<td>" . $row['DataFine'] . "</td>";
      echo "<td>" . $row['DataRitorno'] . "</td>";
      echo "<td>" . $row['Deposito'] . "</td>";
      echo "<td>" . $row['Pagamento'] . "</td>";
      echo "<td>" . $row['CostoTotale'] . "</td>";
      // echo "</th>";
      echo "</tr>";
     }

     echo "</tbody></table>";


      $selectinterventi->close();

    $conn->close();

}
?>
