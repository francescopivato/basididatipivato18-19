<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }
//
// print_r($_POST);

if (isset($_POST['clienteselect']) &&
    isset($_POST['dispositivoselect']) &&
    isset($_POST['interventoselect']) &&
    isset($_POST['pagamento']))   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['clienteselect'];
    $iddispositivo = (int) $_POST['dispositivoselect'];
    $idintervento = (int) $_POST['interventoselect'];
    $pagamento = (double) $_POST['pagamento'];
    $data = date("Y-m-d"); // useremo questa variabile sia per interventi.DataRitorno sia per pagamenti.Data

    function inserisciPagamento($conn, $idcliente, $iddispositivo, $idintervento, $pagamento, $data){

      // require("db_connection.php");
      // require("use_db.php");

        $stmt = $conn->prepare("INSERT INTO pagamenti (Data, Importo)
                                  VALUES (?, ?)");

        //bind_param
        $stmt->bind_param("sd", $data, $pagamento);

        if ($stmt->execute() == TRUE) {

          $selectid = $conn->prepare("SELECT LAST_INSERT_ID() FROM pagamenti");
          $selectid->execute();
          $result = $selectid->get_result();

          if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            $idpagamento = $row['LAST_INSERT_ID()']; // restituisce id corrente


              $inserimentopagamento = $conn->prepare("UPDATE
                                                          interventi
                                                      SET
                                                          Pagamento = ?,
                                                          DataRitorno = ?
                                                      WHERE
                                                          Cliente = ? AND
                                                          Dispositivo = ? AND
                                                          IdIntervento = ?  ");


              $inserimentopagamento->bind_param("isiii", $idpagamento, $data, $idcliente, $iddispositivo, $idintervento);

              if ($inserimentopagamento->execute() == TRUE) {

                echo "<script>
                alert('Pagamento inserito correttamente');
                window.location.href='home.php';
                </script>";

              } else {
                echo $inserimentopagamento->error;
              }


          } else {
                  echo $stmt->error;
        }

        $stmt->close();

      }
      $conn->close();

    } // chiusura function inserisciPagamento()



    $selectdata = $conn->prepare("SELECT
                                          	DataFine, Pagamento
                                          FROM
                                              interventi
                                          WHERE
                                              Cliente = ?
                                          AND Dispositivo = ?
                                          AND IdIntervento = ?");

    $selectdata->bind_param("iii", $idcliente, $iddispositivo, $idintervento);
    $selectdata->execute();
    $result = $selectdata->get_result();
    $row = $result->fetch_assoc();

    $checkdatafine = $row['DataFine'];
    $checkpagamento = $row['Pagamento'];

    // echo "result is ";
    // print_r($row);

    if ($checkdatafine) { // se la data di fine esiste allora FORSE si puo fare il pagamento
                          // ma prima di farlo bisogna controllare che non sia gia stato fatto

      // echo "data di fine esiste, controlliamo se esiste il pagamento";

      if (!$checkpagamento) { // se il pagamento non esiste, allora FORSE si puo fare il pagamento
                              // ma prima bisogna controllare che il pagamento sia la cifra esatta richiesta
        // echo "il pagamento non esiste quindi lo inseriamo";

        $selectinterventi = $conn->prepare("SELECT
                                                D.Importo AS ImportoDeposito,
                                                I.CostoTotale As CostoTotale
                                            FROM
                                                interventi I
                                            INNER JOIN depositi D ON
                                            	I.Deposito = D.IdDeposito
                                            WHERE
                                                Cliente = ? AND Dispositivo = ? AND IdIntervento = ?");
        $selectinterventi->bind_param("iii", $idcliente, $iddispositivo, $idintervento);
        $selectinterventi->execute();
        $result = $selectinterventi->get_result();
        //
        // echo "result is ";
        //
        // print_r($result);


        $row = $result->fetch_assoc();
        //
        // echo "row is ";
        //
        // print_r($row);
        //
        // echo "pagamento = " . $pagamento;

        $costototale = (double) $row['CostoTotale'];
        $importodeposito = (double) $row['ImportoDeposito'];
        $importodapagare = $costototale - $importodeposito;

        // echo "importodapafare = " . $importodapagare;

        $selectinterventi->close();

        if (round($pagamento,2) != round($importodapagare,2)) { // se il pagamento inserito non corrisponde
                                                                //alla cifra richiesta, annulla il pagamento
                                                                // ahime' mi tocca usare round perche non si possono
                                                                // confrontare due var float
          // echo "pagamento non corrispondente";
          echo "<script>
          alert('Importo inserito non corretto. Pagamento annullato. Importo inserito: " . $pagamento . " Importo richiesto: " . $importodapagare . "');
          window.location.href='home.php';
          </script>";
        } else {
          inserisciPagamento($conn, $idcliente, $iddispositivo, $idintervento, $pagamento, $data);
        }



      } else {
        echo "<script>
        alert('Il pagamento risulta già effettuato. Pagamento annullato.');
        window.location.href='interventi.php';
        </script>";
      }


    } else { // altrimenti se la data di fine non è settata significa che quell'intervento è ancora aperto
              // quindi non si  puo effettuare il pagamento

              echo "<script>
              alert('Per pagare questo intervento è necessario che venga prima terminato. Pagamento annullato. IdCliente: " . $idcliente . " IdDispositivo: " . $iddispositivo . " IdIntervento: " . $idintervento . "');
              window.location.href='interventi.php';
              </script>";

    }
    $selectdata->close();

}
?>
