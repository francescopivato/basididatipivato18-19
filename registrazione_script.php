        <?php

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        if (isset($_POST['nome']) &&
            isset($_POST['cognome']) &&
            isset($_POST['email']) &&
            isset($_POST['telefono']) &&
            isset($_POST['password']) &&
            isset($_POST['via']) &&
            isset($_POST['numerocivico']) &&
            isset($_POST['citta']) &&
            isset($_POST['cap']) &&
            isset($_POST['dataassunzione']) &&
            isset($_POST['tipocontratto']) &&
            isset($_POST['stipendio']))   {

            require("db_connection.php");
            require("use_db.php");

            $nome = $_POST['nome'];
            $cognome = $_POST['cognome'];
            $email = $_POST['email'];
            $telefono = (int) $_POST['telefono'];
            $password = $_POST['password'];
            $via = $_POST['via'];
            $numerocivico = $_POST['numerocivico'];
            $citta = $_POST['citta'];
            $cap = $_POST['cap'];
            $dataassunzione = $_POST['dataassunzione'];
            $tipocontratto = $_POST['tipocontratto'];
            $stipendio = (int) $_POST['stipendio'];

            if (isset($_POST['cf'])) {
              $cf = $_POST['cf'];
            } else {
              $cf = NULL;
            }




              $stmt = $conn->prepare("INSERT INTO tecnici (Password, CF, Cognome, Nome, Email, Telefono, IndVia, IndNumero, IndCitta, IndCap, DataAssunzione, TipoContratto, Stipendio)
                                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");


              $stmt->bind_param("sssssissssssi", $password, $cf, $cognome, $nome, $email, $telefono, $via, $numerocivico, $citta, $cap, $dataassunzione, $tipocontratto, $stipendio);

              if ($stmt->execute() == TRUE) {

                $selectid = $conn->prepare("SELECT LAST_INSERT_ID() FROM tecnici");
                $selectid->execute();
                $result = $selectid->get_result();

                if ($result->num_rows > 0){
                  $row = $result->fetch_assoc();
                  $idcorrente = $row['LAST_INSERT_ID()']; // restituisce id corrente
                  echo "<script>
                  alert('Il tuo ID Tecnico per accedere è: " . $idcorrente . "');
                  window.location.href='login.php';
                  </script>";
                }
              } else {
                echo $stmt->error;
              }


              $stmt->close();

            $conn->close();

        }
        ?>
