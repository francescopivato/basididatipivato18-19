<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Tecnici</title>

    <script type="text/javascript">

    function mostraInserisci(){
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#inserisci").toggle();
      window.location.href = "registrazione.php";
    }

    function mostraModifica(){
      $("#inserisci").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#modifica").toggle();
    }

    function mostraElimina(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#visualizza").hide();
      $("#elimina").toggle();
    }

    function mostraVisualizza(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").toggle();
    }


    </script>


</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="home.php">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="home.php">Home</a>
                <a class="nav-item nav-link" href="clienti.php">Clienti</a>
                <a class="nav-item nav-link" href="dispositivi.php">Dispositivi</a>
                <a class="nav-item nav-link" href="interventi.php">Interventi</a>
                <a class="nav-item nav-link" href="operazioni.php">Operazioni</a>
                <a class="nav-item nav-link" href="pagamenti.php">Pagamenti</a>
                <a class="nav-item nav-link" href="#">Tecnici</a>
                <a class="nav-item nav-link" href="acquisti.php">Acquisti</a>
                <a class="nav-item nav-link" href="categorie.php">Categorie</a>
                <a class="nav-item nav-link" href="elementi.php">Elementi</a>
                <a class="nav-item nav-link" href="utilizzi.php">Utilizzi</a>
                <a class="nav-item nav-link" href="logout.php">Logout</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row buttons">
              <div class="offset-sm-2 col-sm-8">
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraInserisci()">Inserisci i dati di un tecnico</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraModifica()">Modifica i dati di un tecnico (X)</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraElimina()">Elimina un tecnico (X)</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraVisualizza()">Visualizza i dati di tutti i tecnici</button>
              </div>
            </div>
            <div class="row actions">
              <div class="col-sm-12">
                <div class="inserisci initiallyhidden" id="inserisci">

                </div>
                <div class="modifica initiallyhidden" id="modifica">

                </div>
                <div class="elimina initiallyhidden" id="elimina">
                </div>
                <div class="visualizza initiallyhidden mt-3" id="visualizza">
                  <?php

                      require("db_connection.php");
                      require("use_db.php");


                      $selecttecnici = $conn->prepare("SELECT * FROM tecnici");
                      $selecttecnici->execute();
                      $result = $selecttecnici->get_result();

                      echo "<table class=\"table\">
                      <thead>
                      <tr>
                      <th scope=\"col\">IdTecnico</th>
                      <th scope=\"col\">CF</th>
                      <th scope=\"col\">Cognome</th>
                      <th scope=\"col\">Nome</th>
                      <th scope=\"col\">Email</th>
                      <th scope=\"col\">Telefono</th>
                      <th scope=\"col\">Via</th>
                      <th scope=\"col\">Numero</th>
                      <th scope=\"col\">Città</th>
                      <th scope=\"col\">CAP</th>
                      <th scope=\"col\">Data Assunzione</th>
                      <th scope=\"col\">Contratto</th>
                      <th scope=\"col\">Stipendio</th>
                      <th scope=\"col\">Attivo</th>
                      </tr>
                      </thead>
                      <tbody>";

                      while ($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        // echo "<th scope=\"row\">";
                        echo "<td>" . $row['IdTecnico'] . "</td>";
                        echo "<td>" . $row['CF'] . "</td>";
                        echo "<td>" . $row['Cognome'] . "</td>";
                        echo "<td>" . $row['Nome'] . "</td>";
                        echo "<td>" . $row['Email'] . "</td>";
                        echo "<td>" . $row['Telefono'] . "</td>";
                        echo "<td>" . $row['IndVia'] . "</td>";
                        echo "<td>" . $row['IndNumero'] . "</td>";
                        echo "<td>" . $row['IndCitta'] . "</td>";
                        echo "<td>" . $row['IndCap'] . "</td>";
                        echo "<td>" . $row['DataAssunzione'] . "</td>";
                        echo "<td>" . $row['TipoContratto'] . "</td>";
                        echo "<td>" . $row['Stipendio'] . "</td>";
                        echo "<td>" . $row['Attivo'] . "</td>";
                        // echo "</th>";
                        echo "</tr>";
                       }

                       echo "</tbody></table>";


                        $selecttecnici->close();


                      $conn->close();

                  ?>
              </div>
            </div>
            </div>
          </main>
        </div>
      </div>
    </div>
</body>
</html>
