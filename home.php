<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Home</title>
</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#">Home</a>
                <a class="nav-item nav-link" href="clienti.php">Clienti</a>
                <a class="nav-item nav-link" href="dispositivi.php">Dispositivi</a>
                <a class="nav-item nav-link" href="interventi.php">Interventi</a>
                <a class="nav-item nav-link" href="operazioni.php">Operazioni</a>
                <a class="nav-item nav-link" href="pagamenti.php">Pagamenti</a>
                <a class="nav-item nav-link" href="tecnici.php">Tecnici</a>
                <a class="nav-item nav-link" href="acquisti.php">Acquisti</a>
                <a class="nav-item nav-link" href="categorie.php">Categorie</a>
                <a class="nav-item nav-link" href="elementi.php">Elementi</a>
                <a class="nav-item nav-link" href="utilizzi.php">Utilizzi</a>
                <a class="nav-item nav-link" href="logout.php">Logout</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="offset-sm-4 col-sm-4 mt-5">
          <h2>Benvenuto, <?php echo nl2br($_SESSION['Nome'] . " " .  $_SESSION['Cognome'] . " \n(Id Tecnico: " . $_SESSION['IdTecnico'] . ")"); ?></h2>
        </div>
        <div class="col-sm-12">
          <main>
            <div class="row links">
              <div class="offset-sm-2 col-sm-8">
                <div class="btn-toolbar btn-group-vertical" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group-vertical mb-5 mt-5 btn-block" role="group" aria-label="First group">
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'clienti.php';">Clienti</button>
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'dispositivi.php';">Dispositivi</button>
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'interventi.php';">Interventi</button>
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'operazioni.php';">Operazioni</button>
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'pagamenti.php';">Pagamenti</button>
                    </div>
                    <div class="btn-group-vertical mb-5 btn-block" role="group" aria-label="Second group">
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'tecnici.php';">Tecnici</button>
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'acquisti.php';">Acquisti</button>
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'categorie.php';">Categorie</button>
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'elementi.php';">Elementi</button>
                      <button type="submit" class="btn btn-outline-light" onclick="location.href = 'utilizzi.php';">Utilizzi</button>
                    </div>
                </div>
            </div>
            </div>
          </main>
        </div>
      </div>
        <div class="row footerlongpage">
          <div class="col-sm-12">
            <footer>
              Sito sviluppato da Pivato Francesco
            </footer>
          </div>
        </div>
    </div>
</body>
</html>
