<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['clienteselect']) )   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['clienteselect'];

  // in caso di emergenza decommentare (elimina solo clienti e dispositivi che non hanno interventi)
  // e commentare tutto il resto

  //   $deleteclienti = $conn->prepare("DELETE FROM clienti WHERE IdCliente=?"); // non opera a cascata
  //
  //   $deleteclienti->bind_param("i", $idcliente);
  //
  //   if ($deleteclienti->execute() == TRUE) {
  //       echo "<script>
  //       alert('Cliente eliminato con successo!');
  //       window.location.href='home.php';
  //       </script>";
  //
  //   } else {
  //     echo $deleteclienti->error;
  //   }
  //
  //   $deleteclienti->close();
  //
  // $conn->close();

    $deleteoperazioni = $conn->prepare("DELETE
                                          FROM
                                              operazioni
                                          WHERE
                                              Cliente = ?");

    $deleteoperazioni->bind_param("i", $idcliente);

    if ($deleteoperazioni->execute() == TRUE) {

      $deleteutilizzi = $conn->prepare("DELETE
                                            FROM
                                              utilizzi
                                            WHERE
                                              Cliente = ?");

      $deleteutilizzi->bind_param("i", $idcliente);


      if ($deleteutilizzi->execute() == TRUE) {

        $updatedepositipagamenti = $conn->prepare("UPDATE interventi
                                            SET
                                              Deposito = NULL,
                                              Pagamento = NULL
                                            WHERE
                                              Cliente = ? ");

        $updatedepositipagamenti->bind_param("i", $idcliente);

        if ($updatedepositipagamenti->execute() == TRUE) {

          $deletepagamenti = $conn->prepare("DELETE
                                                  P
                                              FROM
                                                  pagamenti P
                                              LEFT OUTER JOIN interventi I ON
                                                  P.IdPagamento = I.Pagamento
                                              WHERE
                                                Cliente IS NULL");

          if ($deletepagamenti->execute() == TRUE) {

            $deletedepositi = $conn->prepare("DELETE
                                                D
                                              FROM
                                                depositi D
                                              LEFT OUTER JOIN interventi I ON
                                                D.IdDeposito = I.Deposito
                                              WHERE
                                                Cliente IS NULL");


            if ($deletedepositi->execute() == TRUE) {

              $deleteinterventi = $conn->prepare("DELETE
                                                  I
                                                FROM
                                                  interventi I
                                                WHERE
                                                  Cliente = ?");

              $deleteinterventi->bind_param("i", $idcliente);

              if ($deleteinterventi->execute() == TRUE) {


                $deletedispositivi = $conn->prepare("DELETE
                                                        D
                                                      FROM
                                                        Dispositivi D
                                                      WHERE
                                                        Cliente = ?");

                $deletedispositivi->bind_param("i", $idcliente);


                if ($deletedispositivi->execute() == TRUE) {

                  $deleteclienti = $conn->prepare("DELETE FROM clienti WHERE IdCliente=?");

                  $deleteclienti->bind_param("i", $idcliente);

                  if ($deleteclienti->execute() == TRUE) {
                      echo "<script>
                      alert('Cliente eliminato con successo!');
                      window.location.href='home.php';
                      </script>";

                  } else {
                    echo $deleteclienti->error;
                  }

                  $deleteclienti->close();


                } else {
                  echo $deletedispositivi->error;
                }

                $deletedispositivi->close();

              } else {
                echo $deleteinterventi->error;
              }

              $deleteinterventi->close();

            } else {
              echo $deletedepositi->error;
            }

            $deletedepositi->close();

          } else {
            echo $deletepagamenti->error;
          }

          $deletepagamenti->close();

        } else {
          echo $updatedepositipagamenti->error;
        }

        $updatedepositipagamenti->close();

      } else {
        echo $deleteutilizzi->error;
      }

      $deleteutilizzi->close();


    } else {
      echo $deleteoperazioni->error;
    }

    $deleteoperazioni->close();

    $conn->close();

}
?>
