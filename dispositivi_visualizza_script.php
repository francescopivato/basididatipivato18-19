<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['id']) )   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['id'];

    $selectdispositivi = $conn->prepare("SELECT * FROM dispositivi WHERE Cliente=?");
    $selectdispositivi->bind_param("i", $idcliente);
    $selectdispositivi->execute();
    $result = $selectdispositivi->get_result();

    echo "<table class=\"table\">
    <thead>
    <tr>
    <th scope=\"col\">IdDispositivo</th>
    <th scope=\"col\">Seriale</th>
    <th scope=\"col\">Marca</th>
    <th scope=\"col\">Modello</th>
    </tr>
    </thead>
    <tbody>";

    while ($row = $result->fetch_assoc()) {
      echo "<tr>";
      // echo "<th scope=\"row\">";
      echo "<td>" . $row['IdDispositivo'] . "</td>";
      echo "<td>" . $row['NumSeriale'] . "</td>";
      echo "<td>" . $row['Marca'] . "</td>";
      echo "<td>" . $row['Modello'] . "</td>";
      // echo "</th>";
      echo "</tr>";
     }

     echo "</tbody></table>";



      $selectdispositivi->close();

    $conn->close();

}
?>
