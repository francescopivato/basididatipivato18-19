<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['id']) )   {

    require("db_connection.php");
    require("use_db.php");

    $idcat = (int) $_POST['id'];

    $selectelementi = $conn->prepare("SELECT * FROM elementi WHERE Categoria=?");
    $selectelementi->bind_param("i", $idcat);
    $selectelementi->execute();
    $result = $selectelementi->get_result();

    echo "<table class=\"table\">
    <thead>
    <tr>
    <th scope=\"col\">IdElemento</th>
    <th scope=\"col\">Nome</th>
    <th scope=\"col\">Costo</th>
    <th scope=\"col\">Scorte</th>
    </tr>
    </thead>
    <tbody>";

    while ($row = $result->fetch_assoc()) {
      echo "<tr>";
      // echo "<th scope=\"row\">";
      echo "<td>" . $row['IdElemento'] . "</td>";
      echo "<td>" . $row['Nome'] . "</td>";
      echo "<td>" . $row['Costo'] . "</td>";
      echo "<td>" . $row['Scorte'] . "</td>";
      // echo "</th>";
      echo "</tr>";
     }

     echo "</tbody></table>";


      $selectelementi->close();

    $conn->close();

}
?>
