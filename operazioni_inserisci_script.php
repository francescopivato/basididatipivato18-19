<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }
//
// print_r($_POST);

//se categoria, elemento e quantita non sono stati selezionati non compariranno nella variabile post
// $_POST['categoriaselect']
// $_POST['elementoselect']
// $_POST['quantita']

if (isset($_POST['clienteselect']) &&
    isset($_POST['dispositivoselect']) &&
    isset($_POST['datainizio']) &&
    isset($_POST['datafine']) &&
    isset($_POST['commento']))   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['clienteselect'];
    $iddispositivo = (int) $_POST['dispositivoselect'];
    $datainizio = $_POST['datainizio'];
    $datafine = $_POST['datafine'];
    $commento = $_POST['commento'];
    $idtecnico = (int) $_SESSION['IdTecnico'];

    $selectidintervento = $conn->prepare("SELECT
                                            IdIntervento
                                        FROM
                                            interventi
                                        WHERE
                                            Cliente = ?
                                        AND
                                        	Dispositivo = ?
                                        AND
                                        	DataFine IS NULL");

    $selectidintervento->bind_param("ii", $idcliente, $iddispositivo);
    $selectidintervento->execute();
    $result = $selectidintervento->get_result();
    $row = $result->fetch_assoc();
    $idintervento = $row['IdIntervento'];
    $selectidintervento->close();

    if (isset($idintervento)) {

        // echo "\n siamo dentro l'if\n";

        $selectidoperazione = $conn->prepare("SELECT
                                                MAX(IdOperazione)
                                              FROM
                                                operazioni
                                              WHERE
                                                Cliente=?
                                              AND
                                                Dispositivo=?
                                              AND
                                                Intervento=?");
        $selectidoperazione->bind_param("iii", $idcliente, $iddispositivo, $idintervento);
        $selectidoperazione->execute();
        $result = $selectidoperazione->get_result();
        $row = $result->fetch_assoc();
        // //
        // print_r($row);

        if(isset($row['MAX(IdOperazione)'])){
          // echo "bella pe tutti esiste id dispositivo";
          //     echo "Max id is " . $row['MAX(IdDispositivo)'];
              $idoperazione = $row['MAX(IdOperazione)']+1;
        } else {
          // echo "odio il mondo infatti id dispositivo e' null";
          $idoperazione = 1;
        }



        $selectidoperazione->close();

        $scortesufficienti = true; // mi serve dopo aver verificato le scorte. Se le scorte sono inferiori alla quantita richiesta
                                  //  setto la var a false
                                  // e metto in un if tutto il resto dell'operazione inserimento

        if (isset($_POST['categoriaselect']) &&
            isset($_POST['elementoselect']) &&
            isset($_POST['quantita'])) {

              //nel caso in cui sia stato utilizzato, lo aggiungeremo nella tabella utilizzi e modificheremo la tabella elementi

              $categoria = (int) $_POST['categoriaselect'];
              $elemento = (int) $_POST['elementoselect'];
              $quantita = (int) $_POST['quantita'];

              $data = date("Y-m-d");

              //controlliamo che la quantita richiesta non superi le scorte disponibili

              $checkscorte = $conn->prepare("SELECT Scorte FROM elementi WHERE Categoria=? AND IdElemento=?");
              $checkscorte->bind_param("ii", $categoria, $elemento);
              $checkscorte->execute();
              $result = $checkscorte->get_result();
              $row = $result->fetch_assoc();
              $scorteattuali = $row['Scorte'];
              $checkscorte->close();

              if (($scorteattuali - $quantita) >= 0) {

                // selezioniamo il costo dell'elemento e moltiplichiamolo per la quantita usata

                $selectprezzoelemento = $conn->prepare("SELECT Costo FROM elementi WHERE Categoria=? AND IdElemento=?");
                $selectprezzoelemento->bind_param("ii", $categoria, $elemento);
                $selectprezzoelemento->execute();
                $result = $selectprezzoelemento->get_result();
                $row = $result->fetch_assoc();
                $costoelemento = (float) $row['Costo'];

                $costototale = $costoelemento * $quantita;

                $selectprezzoelemento->close();

                $updatescorte = $conn->prepare("UPDATE elementi SET Scorte = Scorte - $quantita WHERE Categoria=? AND IdElemento=?");
                  $updatescorte->bind_param("ii", $categoria, $elemento);
                  $status = $updatescorte->execute();
                  if ($status) {
                    // echo "scorte aggiornate";
                    $stmt = $conn->prepare("INSERT INTO utilizzi (Cliente, Dispositivo, Intervento, Categoria, Elemento, Data, Quantita, CostoTotale)
                                              VALUES (?, ?, ?, ?, ?, ?, ?, ?)");


                    $stmt->bind_param("iiiiisid", $idcliente, $iddispositivo, $idintervento, $categoria, $elemento, $data, $quantita, $costototale);

                    $stmt->execute();

                    $stmt->close();

                  } else {
                    echo "errore scorte";
                  }

              } else { // se scorte - quantita < 0 allora errore e torniamo alla pagina principale
                  echo "<script>
                  alert('Errore: La quantità richiesta per Categoria: " . $categoria . " Elemento: " . $elemento . " è maggiore delle scorte disponibili. Operazione annullata.');
                  window.location.href='home.php';
                  </script>";

                  $scortesufficienti = false;

              }
        }


        //inserimento operazione

        if ($scortesufficienti) {

          $stmt = $conn->prepare("INSERT INTO operazioni (IdOperazione, Cliente, Dispositivo, Intervento,  DataInizio, DataFine, Commento, Tecnico)
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

          //bind_param
          $stmt->bind_param("iiiisssi", $idoperazione, $idcliente, $iddispositivo, $idintervento, $datainizio, $datafine, $commento, $idtecnico);

          if ($stmt->execute() == TRUE) {

            echo "<script>
            alert('Inserimento operazione completato');
            window.location.href='home.php';
            </script>";

          } else {
            echo $stmt->error;
          }

        }

          $stmt->close();

        $conn->close();

    } else {
      echo "<script>
      alert('Errore: Per aggiungere questa operazione è necessario che venga creato un intervento con IdCliente: " . $idcliente . " e IdDispositivo: " . $iddispositivo . "');
      window.location.href='interventi.php';
      </script>";
    }

}
?>
