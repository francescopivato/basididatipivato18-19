<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Utilizzi</title>

    <script type="text/javascript">

    function mostraInserisci(){
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#inserisci").toggle();
      window.location.href = "operazioni.php";

    }

    function mostraModifica(){
      $("#inserisci").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#modifica").toggle();
    }

    function mostraElimina(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#visualizza").hide();
      $("#elimina").toggle();
    }

    function mostraVisualizza(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").toggle();
    }

    function getDispositiviVisualizza(idCliente){
      // console.log("id cliente lato client is " + idCliente);

      $.post("dispositivi_select_script.php", {
        id: idCliente
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#dispositivoselectvisualizza").children().remove();
        $("#dispositivoselectvisualizza").append(data);
      });

    }

    function getInterventiVisualizza(idDispositivo){

      var idCliente = $("#clienteselectvisualizza").children("option:selected").val();

      // console.log("id cliente lato client is " + idCliente);
      // console.log("id dispositivo lato client is " + idDispositivo);

      $.post("interventi_select_script.php", {
        idC: idCliente,
        idD: idDispositivo
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#interventoselectvisualizza").children().remove();
        $("#interventoselectvisualizza").append(data);
      });

    }

    function getUtilizziVisualizza(idIntervento){

      var idCliente = $("#clienteselectvisualizza").children("option:selected").val();
      var idDispositivo = $("#dispositivoselectvisualizza").children("option:selected").val();

      // console.log("id cliente lato client is " + idCliente);
      // console.log("id dispositivo lato client is " + idDispositivo);
      // console.log("id intervento lato client is " + idIntervento);

      $.post("utilizzi_select_script.php", {
        idC: idCliente,
        idD: idDispositivo,
        idI: idIntervento
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#txtUtilizzi").children().remove();
        $("#txtUtilizzi").append(data);
      });

    }

    </script>

</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="home.php">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="home.php">Home</a>
                <a class="nav-item nav-link" href="clienti.php">Clienti</a>
                <a class="nav-item nav-link" href="dispositivi.php">Dispositivi</a>
                <a class="nav-item nav-link" href="interventi.php">Interventi</a>
                <a class="nav-item nav-link" href="operazioni.php">Operazioni</a>
                <a class="nav-item nav-link" href="pagamenti.php">Pagamenti</a>
                <a class="nav-item nav-link" href="tecnici.php">Tecnici</a>
                <a class="nav-item nav-link" href="acquisti.php">Acquisti</a>
                <a class="nav-item nav-link" href="categorie.php">Categorie</a>
                <a class="nav-item nav-link" href="elementi.php">Elementi</a>
                <a class="nav-item nav-link" href="#">Utilizzi</a>
                <a class="nav-item nav-link" href="logout.php">Logout</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row buttons">
              <div class="offset-sm-2 col-sm-8">
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraInserisci()">Inserisci l'utilizzo di un elemento</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraModifica()">Modifica i dati di un utilizzo (X)</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraElimina()">Elimina i dati di un utilizzo (X)</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraVisualizza()">Visualizza gli elementi utilizzati in un intervento</button>
              </div>
            </div>
            <div class="row actions">
              <div class="offset-sm-1 col-sm-10">
                <div class="inserisci initiallyhidden" id="inserisci">
                </div>
                <div class="modifica initiallyhidden" id="modifica">
                </div>
                <div class="elimina initiallyhidden" id="elimina">
                </div>
                <div class="visualizza initiallyhidden mt-3" id="visualizza">
                  <form action="#" method="post">
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona il proprietario del dispositivo: </legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="clienteselectvisualizza" class="col-sm-2 col-form-label sr-only">Cliente: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="clienteselectvisualizza" id="clienteselectvisualizza" placeholder="Cliente" size="8" onchange="getDispositiviVisualizza(this.value)" required >
                            <?php

                              require("db_connection.php");
                              require("use_db.php");

                              $selectclienti = $conn->prepare("SELECT IdCliente, Cognome, Nome, Email FROM clienti");
                              $selectclienti->execute();
                              $selectclienti->bind_result($id, $cognome, $nome, $email);

                              while ($selectclienti->fetch()) {
                                echo "<option value=\"" . $id . "\">" . $id . " " . $cognome . " " . $nome . " " . $email . "</option>";
                              }

                              $selectclienti->close();

                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                      <fieldset>
                        <div class="row legend">
                          <div class="offset-sm-2 col-sm-8">
                            <legend>Seleziona il dispositivo</legend>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="dispositivoselectvisualizza" class="col-sm-2 col-form-label sr-only">Dispositivo: </label>
                          <div class="offset-sm-2 col-sm-8">
                            <select class="form-control" name="dispositivoselectvisualizza" id="dispositivoselectvisualizza" placeholder="Dispositivo" size="8" onchange="getInterventiVisualizza(this.value)" required >
                            </select>
                          </div>
                        </div>
                      </fieldset>
                      <fieldset>
                        <div class="row legend">
                          <div class="offset-sm-2 col-sm-8">
                            <legend>Seleziona l'intervento</legend>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="interventoselectvisualizza" class="col-sm-2 col-form-label sr-only">Intervento: </label>
                          <div class="offset-sm-2 col-sm-8">
                            <select class="form-control" name="interventoselectvisualizza" id="interventoselectvisualizza" placeholder="Intervento" size="8" onchange="getUtilizziVisualizza(this.value)"required >
                            </select>
                          </div>
                        </div>
                      </fieldset>
                  </form>
                  <div id="txtUtilizzi" class="col-sm-12"></div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
</body>
</html>
