<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  // print_r($_POST);

  if (isset($_POST['idC']) &&
      isset($_POST['idD'])) {



    require("db_connection.php");
    require("use_db.php");

    $idCliente = (int)($_POST['idC']);
    $idDispositivo = (int)($_POST['idD']);

    $selectinterventi = $conn->prepare("SELECT
                                            I.IdIntervento,
                                            I.DataRicezione,
                                            I.DataFine,
                                            I.DataRitorno,
                                            D.Importo,
                                            I.CostoTotale
                                        FROM
                                            interventi I
                                        INNER JOIN depositi D ON
                                        	I.Deposito = D.IdDeposito
                                        WHERE
                                            Cliente = ? AND Dispositivo = ?");
    $selectinterventi->bind_param("ii", $idCliente, $idDispositivo);
    $selectinterventi->execute();
    $result = $selectinterventi->get_result();
    while ($row = $result->fetch_assoc()) {

      $importodapagare = $row['CostoTotale'] - $row['Importo']; // $row['Importo'] = si tratta dell'importo del deposito

       // echo "" . $row['IdDispositivo'] . " " . $row['Marca'] . " " . $row['Modello'] . "\n";
       echo "<option value=\"" . $row['IdIntervento'] . "\">" . $row['IdIntervento'] . " Inizio: " . $row['DataRicezione'] . " Fine: " . $row['DataFine'] . " Consegna: " . $row['DataRitorno'] . " Importo: " . $importodapagare . "</option>";

     }

    $selectinterventi->close();

  }

?>
