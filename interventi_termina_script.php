<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

// print_r($_POST);

if (isset($_POST['clienteselecttermina']) &&
    isset($_POST['dispositivoselecttermina']))   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['clienteselecttermina'];
    $iddispositivo = (int) $_POST['dispositivoselecttermina'];
    $datafine = date("Y-m-d");

    $costomanodopera = 24.99;

    $selectidintervento = $conn->prepare("SELECT
                                            IdIntervento
                                        FROM
                                            interventi
                                        WHERE
                                            Cliente = ?
                                        AND
                                        	Dispositivo = ?
                                        AND
                                        	DataFine IS NULL");

    $selectidintervento->bind_param("ii", $idcliente, $iddispositivo);
    $selectidintervento->execute();
    $result = $selectidintervento->get_result();
    $row = $result->fetch_assoc();
    $idintervento = $row['IdIntervento'];
    $selectidintervento->close();

    // calcolo il costo totale dell'intervento


    if (isset($idintervento)) {

        // echo "\n siamo dentro l'if\n";

        $selectoperazionitotali = $conn->prepare("SELECT
                                                      COUNT(IdOperazione) AS OperazioniTotali
                                                  FROM
                                                      operazioni
                                                  WHERE
                                                    Cliente = ?
                                                      AND
                                                      Dispositivo = ?
                                                      AND
                                                      Intervento = ?");

        $selectoperazionitotali->bind_param("iii", $idcliente, $iddispositivo, $idintervento);
        $selectoperazionitotali->execute();
        $result = $selectoperazionitotali->get_result();
        $row = $result->fetch_assoc();
        $operazionitotali = $row['OperazioniTotali'];
        $selectoperazionitotali->close();


        if ($operazionitotali > 0) {

          // echo "operazioni totali = " . $operazionitotali;

          $costoparziale = $operazionitotali * $costomanodopera; // mancano da considerare gli utilizzi

          // echo "costo parziale = " . $costoparziale;

          // controlliamo SE sono stati usati elementi, in che quantita e il costototale

          $selectelementicosto = $conn->prepare("SELECT
                                                        ROUND(SUM(CostoTotale),2) AS CostoTotaleUtilizzi
                                                    FROM
                                                        utilizzi
                                                    WHERE
                                                      Cliente = ?
                                                        AND
                                                        Dispositivo = ?
                                                        AND
                                                        Intervento = ?");

          $selectelementicosto->bind_param("iii", $idcliente, $iddispositivo, $idintervento);
          $selectelementicosto->execute();
          $result = $selectelementicosto->get_result();
          $row = $result->fetch_assoc();
          $costoutilizzi = $row['CostoTotaleUtilizzi'];
          $selectelementicosto->close();
          $costototale;

          if ($costoutilizzi) { // se ci sono utilizzi, somma il loro costo totale al costo

            // echo "ci sono utilizzi. costo utilizzi = " . $costoutilizzi;

            $costototale = $costoparziale + $costoutilizzi;
          } else {
            $costototale = $costoparziale; // altrimenti il costo totale sara composto solo dalle operazioni
          }

          // echo "costo totale senza iva = " .$costototale;

          $iva = round($costototale*22/100, 2); // calcolo dell'iva

          // echo "iva = " . $iva;

          $costototale = $costototale + $iva;

          // echo "siamo arrivati in fondo, il costo totale è = " . $costototale;





            $stmt = $conn->prepare("UPDATE
                                      interventi
                                    SET
                                      DataFine=?, CostoTotale=?
                                    WHERE
                                      Cliente=?
                                    AND
                                      Dispositivo=?
                                    AND
                                      IdIntervento=?");


            $stmt->bind_param("sdiii", $datafine, $costototale, $idcliente, $iddispositivo, $idintervento);

            if ($stmt->execute() == TRUE) {

              // echo "inserimento terminato";

              echo "<script>
              alert('Intervento terminato');
              window.location.href='home.php';
              </script>";

            } else {
              echo $stmt->error;
            }


            $stmt->close();


          $conn->close();

        } else {

          echo "<script>
          alert('Errore: in questo intervento non è stata effettuata alcuna operazione. Termina intervento annullato.');
          window.location.href='interventi.php';
          </script>";

        }


    } else {
      echo "<script>
      alert('Errore: Non esiste alcun intervento aperto che contenga IdCliente: " . $idcliente . " e IdDispositivo: " . $iddispositivo . "');
      window.location.href='interventi.php';
      </script>";
    }





}
?>
