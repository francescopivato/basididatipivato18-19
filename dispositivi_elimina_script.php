<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

// echo "benvenuto in inserisci dispositivi";
// //
// print_r($_POST);

if (isset($_POST['cliente']) &&
    isset($_POST['dispositivoselect']))   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['cliente'];
    $iddispositivo = (int) $_POST['dispositivoselect'];

    // in caso di emergenza rompere il vetro (no scherzo decommentare questo e commentare il resto)
    //
    //   $stmt = $conn->prepare("DELETE FROM dispositivi WHERE Cliente=? AND IdDispositivo=?"); // non opera a cascata
    //
    //   $stmt->bind_param("ii", $idcliente, $iddispositivo);
    //
    //   if ($stmt->execute() == TRUE) {
    //       echo "<script>
    //       alert('Dispositivo eliminato con successo!');
    //       window.location.href='home.php';
    //       </script>";
    //
    //   } else {
    //     echo $stmt->error;
    //   }
    //
    //   $stmt->close();
    //
    // $conn->close();

    $deleteoperazioni = $conn->prepare("DELETE
                                          FROM
                                              operazioni
                                          WHERE
                                              Cliente = ? AND Dispositivo = ?");

    $deleteoperazioni->bind_param("ii", $idcliente, $iddispositivo);

    if ($deleteoperazioni->execute() == TRUE) {

      $deleteutilizzi = $conn->prepare("DELETE
                                            FROM
                                              utilizzi
                                            WHERE
                                              Cliente = ? AND Dispositivo = ?");

      $deleteutilizzi->bind_param("ii", $idcliente, $iddispositivo);


      if ($deleteutilizzi->execute() == TRUE) {

        $updatedepositipagamenti = $conn->prepare("UPDATE interventi
                                            SET
                                              Deposito = NULL,
                                              Pagamento = NULL
                                            WHERE
                                              Cliente = ? AND Dispositivo = ?");

        $updatedepositipagamenti->bind_param("ii", $idcliente, $iddispositivo);

        if ($updatedepositipagamenti->execute() == TRUE) {

          $deletepagamenti = $conn->prepare("DELETE
                                                  P
                                              FROM
                                                  pagamenti P
                                              LEFT OUTER JOIN interventi I ON
                                                  P.IdPagamento = I.Pagamento
                                              WHERE
                                                Cliente IS NULL AND Dispositivo IS NULL");

          if ($deletepagamenti->execute() == TRUE) {

            $deletedepositi = $conn->prepare("DELETE
                                                D
                                              FROM
                                                depositi D
                                              LEFT OUTER JOIN interventi I ON
                                                D.IdDeposito = I.Deposito
                                              WHERE
                                                Cliente IS NULL AND Dispositivo IS NULL");


            if ($deletedepositi->execute() == TRUE) {

              $deleteinterventi = $conn->prepare("DELETE
                                                  I
                                                FROM
                                                  interventi I
                                                WHERE
                                                  Cliente = ? AND Dispositivo = ?");

              $deleteinterventi->bind_param("ii", $idcliente, $iddispositivo);

              if ($deleteinterventi->execute() == TRUE) {


                $deletedispositivi = $conn->prepare("DELETE
                                                        D
                                                      FROM
                                                        Dispositivi D
                                                      WHERE
                                                        Cliente = ?");

                $deletedispositivi->bind_param("i", $idcliente);


                if ($deletedispositivi->execute() == TRUE) {

                      echo "<script>
                      alert('Dispositivo eliminato con successo!');
                      window.location.href='home.php';
                      </script>";



                } else {
                  echo $deletedispositivi->error;
                }

                $deletedispositivi->close();

              } else {
                echo $deleteinterventi->error;
              }

              $deleteinterventi->close();

            } else {
              echo $deletedepositi->error;
            }

            $deletedepositi->close();

          } else {
            echo $deletepagamenti->error;
          }

          $deletepagamenti->close();

        } else {
          echo $updatedepositipagamenti->error;
        }

        $updatedepositipagamenti->close();

      } else {
        echo $deleteutilizzi->error;
      }

      $deleteutilizzi->close();


    } else {
      echo $deleteoperazioni->error;
    }

    $deleteoperazioni->close();

    $conn->close();

}
?>
