<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

// print_r($_POST);

if (isset($_POST['categoriaselect']) &&
    isset($_POST['elementoselect']) &&
    isset($_POST['quantita']))   {

    require("db_connection.php");
    require("use_db.php");

    $idcategoria = (int) $_POST['categoriaselect'];
    $idelemento = (int) $_POST['elementoselect'];
    $quantita = (int) $_POST['quantita'];
    $idtecnico = (int) $_SESSION['IdTecnico'];
    $data = date("Y-m-d");

    //calcolo del costo totale

    $selectprezzoelemento = $conn->prepare("SELECT Costo FROM elementi WHERE Categoria=? AND IdElemento=?");
    $selectprezzoelemento->bind_param("ii", $idcategoria, $idelemento);
    $selectprezzoelemento->execute();
    $result = $selectprezzoelemento->get_result();
    $row = $result->fetch_assoc();
    $costoelemento = $row['Costo'];

    $costototale = $costoelemento * $quantita;

    // echo $costototale;

    // echo "numero dispositivi " . $row['NumeroDispositivi'];

      // echo "il nuovo dispositivo ha il seguente id: " . $iddispositivo;

    $selectprezzoelemento->close();


      $stmt = $conn->prepare("INSERT INTO acquisti (Categoria, Elemento, Tecnico, Data, Quantita, CostoTotale)
                                VALUES (?, ?, ?, ?, ?, ?)");


      $stmt->bind_param("iiisid", $idcategoria, $idelemento, $idtecnico, $data, $quantita, $costototale);

      $stmt->execute();

            $stmt->close();

        $stmt2 = $conn->prepare("SELECT Scorte FROM elementi WHERE Categoria=? AND IdElemento=?");
        $stmt2->bind_param("ii", $idcategoria, $idelemento);
        $stmt2->execute();
        $result2 = $stmt2->get_result();
        $row2 = $result2->fetch_assoc();
        // print_r($row2);
        // echo "scorte is " . $row2['Scorte'];

        $scorte = $row2['Scorte'];

        $scortetotali = $scorte+$quantita;

        $stmt2->close();

        // echo "scortetotali is " . $scortetotali;
        // echo "idcategoria is " . $idcategoria;
        // echo "idelemento is " . $idelemento;

        $stmt3 = $conn->prepare("UPDATE elementi SET Scorte=? WHERE Categoria=? AND IdElemento=?");
        if ($stmt3) {
          // echo "tutto regolare";
          $stmt3->bind_param("iii", $scortetotali, $idcategoria, $idelemento);
          $status = $stmt3->execute();

          if ($status) {
            echo "<script>
            alert('Acquisto completato');
            window.location.href='home.php';
            </script>";
          } else {
            $stmt3->error;
          }

        } else {
          echo "il mondo fa schifo";
        }



    $conn->close();

}
?>
