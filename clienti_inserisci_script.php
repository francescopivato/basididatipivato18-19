<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['nome']) &&
    isset($_POST['cognome']) &&
    isset($_POST['email']) &&
    isset($_POST['telefono']) &&
    isset($_POST['via']) &&
    isset($_POST['numerocivico']) &&
    isset($_POST['citta']) &&
    isset($_POST['cap']))   {

    require("db_connection.php");
    require("use_db.php");

    $nome = $_POST['nome'];
    $cognome = $_POST['cognome'];
    $email = $_POST['email'];
    $telefono = (int) $_POST['telefono'];
    $via = $_POST['via'];
    $numerocivico = $_POST['numerocivico'];
    $citta = $_POST['citta'];
    $cap = $_POST['cap'];

    if (isset($_POST['cf'])) {
      $cf = $_POST['cf'];
    } else {
      $cf = NULL;
    }



      $stmt = $conn->prepare("INSERT INTO clienti (CF, Cognome, Nome, Email, Telefono, IndVia, IndNumero, IndCitta, IndCap)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");

      $stmt->bind_param("ssssissss", $cf, $cognome, $nome, $email, $telefono, $via, $numerocivico, $citta, $cap);

      if ($stmt->execute() == TRUE) {

        $selectid = $conn->prepare("SELECT LAST_INSERT_ID() FROM clienti");
        $selectid->execute();
        $result = $selectid->get_result();

        if ($result->num_rows > 0){
          $row = $result->fetch_assoc();
          $idcliente = $row['LAST_INSERT_ID()']; // restituisce id corrente
          echo "<script>
          alert('Il tuo ID Cliente è: " . $idcliente . "');
          window.location.href='home.php';
          </script>";
        }
      } else {
        echo $stmt->error;
      }

      $stmt->close();

    $conn->close();

}
?>
