<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['id']) )   {

    require("db_connection.php");
    require("use_db.php");

    $idtecnico = (int) $_POST['id'];

      $selectacquisti = $conn->prepare("SELECT  C.Nome AS Categoria,
                                                E.Nome AS Elemento,
                                                A.Data,
                                                A.Quantita,
                                                A.CostoTotale
                                        FROM    cat_elementi C
                                                INNER JOIN elementi E
                                                    ON C.IdCat = E.Categoria
                                                INNER JOIN acquisti A
                                                    ON C.IdCat = A.Categoria
                                                    AND E.IdElemento = A.Elemento
                                        WHERE   A.Tecnico = ?");
    $selectacquisti->bind_param("i", $idtecnico);
    $selectacquisti->execute();
    $result = $selectacquisti->get_result();

    echo "<table class=\"table\">
    <thead>
    <tr>
    <th scope=\"col\">Categoria</th>
    <th scope=\"col\">Elemento</th>
    <th scope=\"col\">Data</th>
    <th scope=\"col\">Quantità</th>
    <th scope=\"col\">Costo Totale</th>
    </tr>
    </thead>
    <tbody>";

    while ($row = $result->fetch_assoc()) {
      echo "<tr>";
      // echo "<th scope=\"row\">";
      echo "<td>" . $row['Categoria'] . "</td>";
      echo "<td>" . $row['Elemento'] . "</td>";
      echo "<td>" . $row['Data'] . "</td>";
      echo "<td>" . $row['Quantita'] . "</td>";
      echo "<td>" . $row['CostoTotale'] . "</td>";
      // echo "</th>";
      echo "</tr>";
     }

     echo "</tbody></table>";


      $selectacquisti->close();


    $conn->close();

}
?>
