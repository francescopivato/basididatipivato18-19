<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  // print_r($_POST);

  if (isset($_POST['idC']) &&
      isset($_POST['idD'])) {



    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int)($_POST['idC']);
    $iddispositivo = (int)($_POST['idD']);

    $selectdataricezione = $conn->prepare("SELECT
                                            DataRicezione
                                        FROM
                                           interventi
                                        WHERE
                                            Cliente = ? AND Dispositivo = ? AND DataFine IS NULL");

    $selectdataricezione->bind_param("ii", $idcliente, $iddispositivo);
    $selectdataricezione->execute();
    $result = $selectdataricezione->get_result();
    $row = $result->fetch_assoc();
    // print_r($row);
    $dataricezione = $row['DataRicezione'];
    $selectdataricezione->close();

    //invio indietro l'attributo min che aggiungero' alla data di inizio operazione


    if (isset($dataricezione)) {
      echo $dataricezione; // se il dispositivo e' in intervento, min = data ricezione
    } else {
      echo "\"\""; // altrimenti min e' vuoto
    }

  }

?>
