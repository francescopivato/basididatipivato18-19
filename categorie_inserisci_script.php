<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['nome']))   {

    require("db_connection.php");
    require("use_db.php");

    $nome = $_POST['nome'];


      $stmt = $conn->prepare("INSERT INTO cat_elementi (Nome)
                                VALUES (?)");

      $stmt->bind_param("s", $nome);
      if (  $stmt->execute()) {
        echo "<script>
        alert('Categoria inserita correttamente');
        window.location.href='home.php';
       </script>";
      };

      $stmt->close();

    $conn->close();

}
?>
