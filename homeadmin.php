<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Home</title>

    <script type="text/javascript">

    function mostraInserisci(){
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#inserisci").toggle();
    }

    function mostraModifica(){
      $("#inserisci").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#modifica").toggle();
    }

    function mostraElimina(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#visualizza").hide();
      $("#elimina").toggle();
    }

    function mostraVisualizza(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").toggle();
    }


    </script>

</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="login-header">
            <figure>
              <img class="logodesktop" src="repairitlogo.png" alt="Repair It Logo">
              <figcaption></figcaption>
            </figure>
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#">Home</a>
                <a class="nav-item nav-link" href="logout.php">Logout</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="offset-sm-4 col-sm-4 mt-5">
          <h2>Benvenuto, Amministratore</h2>
        </div>
        <div class="col-sm-12">
          <main>
            <div class="row buttons">
              <div class="offset-sm-2 col-sm-8">
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraInserisci()">Visualizza le scorte che stanno per terminare</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraModifica()">Visualizza i tre clienti più fedeli</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraElimina()">Elimina un tecnico</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraVisualizza()">Visualizza i tecnici più operativi</button>
              </div>
            </div>
            <div class="row actions">
              <div class="col-sm-12">
                <div class="inserisci initiallyhidden" id="inserisci">
                  <div class="offset-sm-2 col-sm-8 mt-3">
                    <?php


                    $selectelementi = $conn->prepare("SELECT C.Nome AS Categoria, E.Nome AS Elemento, E.Costo, E.Scorte FROM elementi E
                                                      INNER JOIN cat_elementi C ON E.Categoria = C.IdCat
                                                      WHERE Scorte <= 5
                                                      ORDER BY Scorte");
                    $selectelementi->execute();
                    $result = $selectelementi->get_result();

                    echo "<table class=\"table\">
                    <thead>
                    <tr>
                    <th scope=\"col\">Categoria</th>
                    <th scope=\"col\">Elemento</th>
                    <th scope=\"col\">Costo</th>
                    <th scope=\"col\">Scorte</th>
                    </tr>
                    </thead>
                    <tbody>";

                    while ($row = $result->fetch_assoc()) {
                      echo "<tr>";
                      // echo "<th scope=\"row\">";
                      echo "<td>" . $row['Categoria'] . "</td>";
                      echo "<td>" . $row['Elemento'] . "</td>";
                      echo "<td>" . $row['Costo'] . "</td>";
                      echo "<td>" . $row['Scorte'] . "</td>";
                      // echo "</th>";
                      echo "</tr>";
                     }

                     echo "</tbody></table>";


                      $selectelementi->close();

                    // $conn->close();


                     ?>
                  </div>
                </div>
                <div class="modifica initiallyhidden" id="modifica">
                  <div class="offset-sm-2 col-sm-8">
                    <?php

                    $selectclienti = $conn->prepare("SELECT I.Cliente, C.Cognome, C.Nome, COUNT(*) AS NumeroInterventi
                                                        FROM interventi I
                                                        INNER JOIN clienti C ON I.Cliente = C.IdCliente
                                                        GROUP BY Cliente
                                                        ORDER BY NumeroInterventi DESC
                                                        LIMIT 3");

                    $selectclienti->execute();
                    $result = $selectclienti->get_result();

                    echo "<table class=\"table\">
                    <thead>
                    <tr>
                    <th scope=\"col\">IdCliente</th>
                    <th scope=\"col\">Cognome</th>
                    <th scope=\"col\">Nome</th>
                    <th scope=\"col\">Numero Interventi</th>
                    </tr>
                    </thead>
                    <tbody>";

                    while ($row = $result->fetch_assoc()) {
                      echo "<tr>";
                      // echo "<th scope=\"row\">";
                      echo "<td>" . $row['Cliente'] . "</td>";
                      echo "<td>" . $row['Cognome'] . "</td>";
                      echo "<td>" . $row['Nome'] . "</td>";
                      echo "<td>" . $row['NumeroInterventi'] . "</td>";
                      // echo "</th>";
                      echo "</tr>";
                     }

                     echo "</tbody></table>";


                      $selectclienti->close();

                    $conn->close();

                    ?>
                  </div>
                </div>
                <div class="elimina initiallyhidden" id="elimina">
                  <form action="tecnici_elimina_script.php" method="post">
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona un tecnico da eliminare: </legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="tecnicoselect" class="col-sm-2 col-form-label sr-only">tecnico: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="tecnicoselect" placeholder="Tecnico" size="8" required >
                            <?php

                              require("db_connection.php");
                              require("use_db.php");

                              $selectclienti = $conn->prepare("SELECT IdTecnico, Cognome, Nome, Email FROM tecnici");
                              $selectclienti->execute();
                              $selectclienti->bind_result($id, $cognome, $nome, $email);

                              while ($selectclienti->fetch()) {
                                echo "<option value=\"" . $id . "\">" . $id . " " . $cognome . " " . $nome . " " . $email . "</option>";
                              }

                              $selectclienti->close();

                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-8">
                        <label for="password" class="col-sm-2 col-form-label sr-only">Password Amministratore: </label>
                          <input type="password" id="password" name="password" class="form-control" placeholder="Password Amministratore" required>
                        </div>
                      </div>
                    </fieldset>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-8">
                        <input class="btn btn-lg btn-secondary" type="submit" id="elimina" value="Elimina">
                        <input class="btn btn-lg btn-secondary" type="reset" id="reset" value="Reset">
                      </div>
                    </div>
                  </form>
                </div>
                <div class="visualizza initiallyhidden mt-3" id="visualizza">
                  <div class="offset-sm-2 col-sm-8">
                    <?php

                    $selectclienti = $conn->prepare("SELECT T.IdTecnico, T.Cognome, T.Nome, COUNT(O.Tecnico) AS NumeroOperazioni
                                                      FROM tecnici T
                                                      LEFT OUTER JOIN operazioni O ON T.IdTecnico = O.Tecnico
                                                      GROUP BY T.IdTecnico
                                                      ORDER BY NumeroOperazioni DESC");

                    $selectclienti->execute();
                    $result = $selectclienti->get_result();

                    echo "<table class=\"table\">
                    <thead>
                    <tr>
                    <th scope=\"col\">Id Tecnico</th>
                    <th scope=\"col\">Cognome</th>
                    <th scope=\"col\">Nome</th>
                    <th scope=\"col\">Numero Operazioni</th>
                    </tr>
                    </thead>
                    <tbody>";

                    while ($row = $result->fetch_assoc()) {
                      echo "<tr>";
                      // echo "<th scope=\"row\">";
                      echo "<td>" . $row['IdTecnico'] . "</td>";
                      echo "<td>" . $row['Cognome'] . "</td>";
                      echo "<td>" . $row['Nome'] . "</td>";
                      echo "<td>" . $row['NumeroOperazioni'] . "</td>";
                      // echo "</th>";
                      echo "</tr>";
                     }

                     echo "</tbody></table>";


                      $selectclienti->close();

                    $conn->close();

                    ?>
                  </div>
              </div>
            </div>
            </div>
          </main>
        </div>
      </div>
        <div class="row footerlongpage">
          <div class="col-sm-12">
            <footer>
              Sito sviluppato da Pivato Francesco
            </footer>
          </div>
        </div>
    </div>
</body>
</html>
