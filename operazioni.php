<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Operazioni</title>

    <script type="text/javascript">

    function mostraInserisci(){
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#inserisci").toggle();
    }

    function mostraModifica(){
      $("#inserisci").hide();
      $("#elimina").hide();
      $("#visualizza").hide();
      $("#modifica").toggle();
    }

    function mostraElimina(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#visualizza").hide();
      $("#elimina").toggle();
    }

    function mostraVisualizza(){
      $("#inserisci").hide();
      $("#modifica").hide();
      $("#elimina").hide();
      $("#visualizza").toggle();
    }

    function getDispositivi(idCliente){

      $.post("dispositivi_select_script.php", {
        id: idCliente
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#dispositivoselect").children().remove();
        $("#dispositivoselect").append(data);
      });

    }

    function controllaDate() {
      var datainizio = document.getElementById("datainizio").value;
      document.getElementById("datafine").value = "";
      document.getElementById("datafine").setAttribute("min",datainizio);
    }

    function getDispositiviVisualizza(idCliente){
      // console.log("id cliente lato client is " + idCliente);

      $.post("dispositivi_select_script.php", {
        id: idCliente
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#dispositivoselectvisualizza").children().remove();
        $("#dispositivoselectvisualizza").append(data);
      });

    }

    function getInterventiVisualizza(idDispositivo){

      var idCliente = $("#clienteselectvisualizza").children("option:selected").val();

      // console.log("id cliente lato client is " + idCliente);
      // console.log("id dispositivo lato client is " + idDispositivo);

      $.post("interventi_select_script.php", {
        idC: idCliente,
        idD: idDispositivo
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#interventoselectvisualizza").children().remove();
        $("#interventoselectvisualizza").append(data);
      });

    }

    function getOperazioniVisualizza(idIntervento){

      var idCliente = $("#clienteselectvisualizza").children("option:selected").val();
      var idDispositivo = $("#dispositivoselectvisualizza").children("option:selected").val();

      // console.log("id cliente lato client is " + idCliente);
      // console.log("id dispositivo lato client is " + idDispositivo);
      // console.log("id intervento lato client is " + idIntervento);

      $.post("operazioni_select_script.php", {
        idC: idCliente,
        idD: idDispositivo,
        idI: idIntervento
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#txtOperazioni").children().remove();
        $("#txtOperazioni").append(data);
      });

    }

    function getElementi(idCat){ // per visualizzare elementi di una categoria
      // console.log("id cliente lato client is " + idCliente);

      $.post("acquisti_select_script.php", {
        id: idCat
      },
      function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        $("#elementoselect").children().remove();
        $("#elementoselect").append(data);
      });

    }

    function makeRequired(){
      $("#categoriaselect").prop('required',true);
      $("#elementoselect").prop('required',true);
      $("#quantita").prop('required',true);
    }

    function disableRequired(){
      $("#categoriaselect").prop('required',false);
      $("#elementoselect").prop('required',false);
      $("#quantita").prop('required',false);
    }

    function getData(dispositivo){ // vado a vedere la data di ricezione intervento
                                  // e setto la data inizio operazione min uguale a quel valore

      var idCliente = $("#clienteselect").children("option:selected").val();
      var idDispositivo = dispositivo;

      // console.log("id cliente " + idCliente + " id dispositivo " + idDispositivo);

      $.post("operazioni_select_data_intervento_script.php", {
        idC: idCliente,
        idD: idDispositivo
      },
      function(data, status){
        // console.log("Data: " + data + "\nStatus: " + status);
        // prendo la data ricezione intervento se esiste intervento oppure stringa vuota
        $("#datainizio").attr("min", data);
        $("#datafine").attr("min", data);
      });

    }

    </script>
</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="home.php">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="home.php">Home</a>
                <a class="nav-item nav-link" href="clienti.php">Clienti</a>
                <a class="nav-item nav-link" href="dispositivi.php">Dispositivi</a>
                <a class="nav-item nav-link" href="interventi.php">Interventi</a>
                <a class="nav-item nav-link" href="#">Operazioni</a>
                <a class="nav-item nav-link" href="pagamenti.php">Pagamenti</a>
                <a class="nav-item nav-link" href="tecnici.php">Tecnici</a>
                <a class="nav-item nav-link" href="acquisti.php">Acquisti</a>
                <a class="nav-item nav-link" href="categorie.php">Categorie</a>
                <a class="nav-item nav-link" href="elementi.php">Elementi</a>
                <a class="nav-item nav-link" href="utilizzi.php">Utilizzi</a>
                <a class="nav-item nav-link" href="logout.php">Logout</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
          <div class="row buttons">
            <div class="offset-sm-2 col-sm-8">
                <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraInserisci()">Inserisci un'operazione</button>
                <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraModifica()">Modifica un'operazione (X)</button>
                <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraElimina()">Elimina un'operazione (X)</button>
                <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraVisualizza()">Visualizza le operazioni di un intervento</button>
            </div>
          </div>
          <div class="row actions">
            <div class="offset-sm-1 col-sm-10">
              <div class="inserisci initiallyhidden" id="inserisci">
                <form action="operazioni_inserisci_script.php" method="post">
                  <fieldset>
                    <div class="row legend">
                      <div class="offset-sm-2 col-sm-8">
                        <legend>Seleziona il cliente: </legend>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="clienteselect" class="col-sm-2 col-form-label sr-only">Cliente: </label>
                      <div class="offset-sm-2 col-sm-8">
                        <select class="form-control" name="clienteselect" id="clienteselect" placeholder="Cliente" size="8" onchange="getDispositivi(this.value)" required >
                          <?php

                            require("db_connection.php");
                            require("use_db.php");

                            $selectclienti = $conn->prepare("SELECT IdCliente, Cognome, Nome, Email FROM clienti");
                            $selectclienti->execute();
                            $selectclienti->bind_result($id, $cognome, $nome, $email);

                            while ($selectclienti->fetch()) {
                              echo "<option value=\"" . $id . "\">" . $id . " " . $cognome . " " . $nome . " " . $email . "</option>";
                            }

                            $selectclienti->close();

                          ?>
                        </select>
                      </div>
                    </div>
                  </fieldset>
                  <fieldset>
                    <div class="row legend">
                      <div class="offset-sm-2 col-sm-8">
                        <legend>Seleziona il dispositivo</legend>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="dispositivoselect" class="col-sm-2 col-form-label sr-only">Dispositivo: </label>
                      <div class="offset-sm-2 col-sm-8">
                        <select class="form-control" name="dispositivoselect" id="dispositivoselect" placeholder="Dispositivo" size="8" onchange="getData(this.value)" required >
                        </select>
                      </div>
                    </div>
                  </fieldset>
                  <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Dati Operazione</legend>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="datainizio" class="col-sm-2 col-form-label sr-only">Data di Inizio: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="date" id="datainizio" name="datainizio" min="" class="form-control" placeholder="Data di Inizio" onchange="controllaDate()" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="datafine" class="col-sm-2 col-form-label sr-only">Data di Fine: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="date" id="datafine" name="datafine" min="" class="form-control" placeholder="Data di Fine" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="commento" class="col-sm-2 col-form-label sr-only">Commento: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="commento" name="commento" class="form-control" placeholder="Commento" required>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona la categoria: </legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="categoriaselect" class="col-sm-2 col-form-label sr-only">Categoria: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="categoriaselect" id="categoriaselect" placeholder="Categoria" size="8" onchange="getElementi(this.value)" onclick="makeRequired()" >
                            <?php

                              require("db_connection.php");
                              require("use_db.php");

                              $selectcat = $conn->prepare("SELECT IdCat, Nome FROM cat_elementi");
                              $selectcat->execute();
                              $selectcat->bind_result($id, $nome);

                              while ($selectcat->fetch()) {
                                echo "<option value=\"" . $id . "\">" . $nome . "</option>";
                              }

                              $selectcat->close();

                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona l'elemento</legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="elementoselect" class="col-sm-2 col-form-label sr-only">Elemento: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="elementoselect" id="elementoselect" placeholder="Elemento" size="8" >
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="quantita" class="col-sm-2 col-form-label sr-only">Quantità: </label>
                        <div class="offset-sm-2 col-sm-8 mt-3">
                          <input type="number" id="quantita" name="quantita" class="form-control" placeholder="Quantità" min="1">
                        </div>
                      </div>
                    </fieldset>
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-8">
                      <input class="btn btn-lg btn-secondary" type="submit" name="inserisci" id="inserisci" value="Inserisci">
                      <input class="btn btn-lg btn-secondary" type="reset" id="reset" value="Reset" onclick="disableRequired()">
                    </div>
                  </div>
                </form>
              </div>
              <div class="modifica initiallyhidden" id="modifica">
              </div>
              <div class="elimina initiallyhidden" id="elimina">
              </div>
              <div class="visualizza initiallyhidden mt-3" id="visualizza">
                <form action="#" method="post">
                  <fieldset>
                    <div class="row legend">
                      <div class="offset-sm-2 col-sm-8">
                        <legend>Seleziona il proprietario del dispositivo: </legend>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="clienteselectvisualizza" class="col-sm-2 col-form-label sr-only">Cliente: </label>
                      <div class="offset-sm-2 col-sm-8">
                        <select class="form-control" name="clienteselectvisualizza" id="clienteselectvisualizza" placeholder="Cliente" size="8" onchange="getDispositiviVisualizza(this.value)" required >
                          <?php

                            require("db_connection.php");
                            require("use_db.php");

                            $selectclienti = $conn->prepare("SELECT IdCliente, Cognome, Nome, Email FROM clienti");
                            $selectclienti->execute();
                            $selectclienti->bind_result($id, $cognome, $nome, $email);

                            while ($selectclienti->fetch()) {
                              echo "<option value=\"" . $id . "\">" . $id . " " . $cognome . " " . $nome . " " . $email . "</option>";
                            }

                            $selectclienti->close();

                          ?>
                        </select>
                      </div>
                    </div>
                  </fieldset>
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona il dispositivo</legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="dispositivoselectvisualizza" class="col-sm-2 col-form-label sr-only">Dispositivo: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="dispositivoselectvisualizza" id="dispositivoselectvisualizza" placeholder="Dispositivo" size="8" onchange="getInterventiVisualizza(this.value)" required >
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona l'intervento</legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="interventoselectvisualizza" class="col-sm-2 col-form-label sr-only">Intervento: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="interventoselectvisualizza" id="interventoselectvisualizza" placeholder="Intervento" size="8" onchange="getOperazioniVisualizza(this.value)"required >
                          </select>
                        </div>
                      </div>
                    </fieldset>
                </form>
                <div id="txtOperazioni" class="col-sm-12"></div>
              </div>
            </div>
          </div>
          </main>
        </div>
      </div>
    </div>
</body>
</html>
