<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Clienti</title>

    <script type="text/javascript">

        function mostraInserisci(){
          $("#modifica").hide();
          $("#elimina").hide();
          $("#visualizza").hide();
          $("#inserisci").toggle();
        }

        function mostraModifica(){
          $("#inserisci").hide();
          $("#elimina").hide();
          $("#visualizza").hide();
          $("#modifica").toggle();
        }

        function mostraElimina(){
          $("#inserisci").hide();
          $("#modifica").hide();
          $("#visualizza").hide();
          $("#elimina").toggle();
        }

        function mostraVisualizza(){
          $("#inserisci").hide();
          $("#modifica").hide();
          $("#elimina").hide();
          $("#visualizza").toggle();
        }

    </script>

</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="home.php">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="home.php">Home</a>
                <a class="nav-item nav-link" href="#">Clienti</a>
                <a class="nav-item nav-link" href="dispositivi.php">Dispositivi</a>
                <a class="nav-item nav-link" href="interventi.php">Interventi</a>
                <a class="nav-item nav-link" href="operazioni.php">Operazioni</a>
                <a class="nav-item nav-link" href="pagamenti.php">Pagamenti</a>
                <a class="nav-item nav-link" href="tecnici.php">Tecnici</a>
                <a class="nav-item nav-link" href="acquisti.php">Acquisti</a>
                <a class="nav-item nav-link" href="categorie.php">Categorie</a>
                <a class="nav-item nav-link" href="elementi.php">Elementi</a>
                <a class="nav-item nav-link" href="utilizzi.php">Utilizzi</a>
                <a class="nav-item nav-link" href="logout.php">Logout</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row buttons">
              <div class="offset-sm-2 col-sm-8">
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraInserisci()">Inserisci i dati di un cliente</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraModifica()">Modifica i dati di un cliente (X)</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraElimina()">Elimina i dati di un cliente</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraVisualizza()">Visualizza i dati di tutti i clienti</button>
              </div>
            </div>
            <div class="row actions">
              <div class="offset-sm-1 col-sm-10">
                <div class="inserisci initiallyhidden" id="inserisci">


                  <form action="clienti_inserisci_script.php" method="post">
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Dati Anagrafici</legend>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="cf" class="col-sm-2 col-form-label sr-only">CF: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="cf" name="cf" class="form-control" placeholder="CF">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="nome" class="col-sm-2 col-form-label sr-only">Nome: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="cognome" class="col-sm-2 col-form-label sr-only">Cognome: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="cognome" name="cognome" class="form-control" placeholder="Cognome" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label sr-only">Email: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="telefono" class="col-sm-2 col-form-label sr-only">Telefono: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="telefono" name="telefono" class="form-control" placeholder="Telefono" maxlength="10" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="via" class="col-sm-2 col-form-label sr-only">Via: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="via" name="via" class="form-control" placeholder="Via" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="numerocivico" class="col-sm-2 col-form-label sr-only">Numero Civico: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="numerocivico" name="numerocivico" class="form-control" placeholder="Numero Civico" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="citta" class="col-sm-2 col-form-label sr-only">Città: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="citta" name="citta" class="form-control" placeholder="Città" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="cap" class="col-sm-2 col-form-label sr-only">CAP: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <input type="text" id="cap" name="cap" class="form-control" placeholder="CAP" maxlength="5" required>
                        </div>
                      </div>
                    </fieldset>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-8">
                        <input class="btn btn-lg btn-secondary" type="submit" id="registra" value="Registra">
                        <input class="btn btn-lg btn-secondary" type="reset" id="reset" value="Reset">
                      </div>
                    </div>
                  </form>

                </div>
                <div class="modifica initiallyhidden" id="modifica">

                </div>
                <div class="elimina initiallyhidden" id="elimina">
                    <form action="clienti_elimina_script.php" method="post">
                      <fieldset>
                        <div class="row legend">
                          <div class="offset-sm-2 col-sm-8">
                            <legend>Seleziona un cliente da eliminare: </legend>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="clienteselect" class="col-sm-2 col-form-label sr-only">Cliente: </label>
                          <div class="offset-sm-2 col-sm-8">
                            <select class="form-control" name="clienteselect" placeholder="Cliente" size="8" required >
                              <?php

                                require("db_connection.php");
                                require("use_db.php");

                                $selectclienti = $conn->prepare("SELECT IdCliente, Cognome, Nome, Email FROM clienti");
                                $selectclienti->execute();
                                $selectclienti->bind_result($id, $cognome, $nome, $email);

                                while ($selectclienti->fetch()) {
                                  echo "<option value=\"" . $id . "\">" . $id . " " . $cognome . " " . $nome . " " . $email . "</option>";
                                }

                                $selectclienti->close();

                              ?>
                            </select>
                          </div>
                        </div>
                      </fieldset>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-8">
                          <input class="btn btn-lg btn-secondary" type="submit" id="elimina" value="Elimina">
                          <input class="btn btn-lg btn-secondary" type="reset" id="reset" value="Reset">
                        </div>
                      </div>
                    </form>

                </div>
                <div class="visualizza initiallyhidden" id="visualizza">
                  <?php

                  require("db_connection.php");
                  require("use_db.php");


                  $selecttecnici = $conn->prepare("SELECT * FROM clienti");
                  $selecttecnici->execute();
                  $result = $selecttecnici->get_result();

                  echo "<table class=\"table\">
                  <thead>
                  <tr>
                  <th scope=\"col\">IdCliente</th>
                  <th scope=\"col\">CF</th>
                  <th scope=\"col\">Cognome</th>
                  <th scope=\"col\">Nome</th>
                  <th scope=\"col\">Email</th>
                  <th scope=\"col\">Telefono</th>
                  <th scope=\"col\">Via</th>
                  <th scope=\"col\">Numero</th>
                  <th scope=\"col\">Città</th>
                  <th scope=\"col\">CAP</th>
                  </tr>
                  </thead>
                  <tbody>";

                  while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    // echo "<th scope=\"row\">";
                    echo "<td>" . $row['IdCliente'] . "</td>";
                    echo "<td>" . $row['CF'] . "</td>";
                    echo "<td>" . $row['Cognome'] . "</td>";
                    echo "<td>" . $row['Nome'] . "</td>";
                    echo "<td>" . $row['Email'] . "</td>";
                    echo "<td>" . $row['Telefono'] . "</td>";
                    echo "<td>" . $row['IndVia'] . "</td>";
                    echo "<td>" . $row['IndNumero'] . "</td>";
                    echo "<td>" . $row['IndCitta'] . "</td>";
                    echo "<td>" . $row['IndCap'] . "</td>";
                    // echo "</th>";
                    echo "</tr>";
                   }

                   echo "</tbody></table>";


                    $selecttecnici->close();

                  // }
                  $conn->close();

                   ?>
            </div>
          </div>
          </div>
          </main>
        </div>
    </div>
    </div>
</body>
</html>
