<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

if (isset($_POST['id']) )   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['id'];

    $selectclienti = $conn->prepare("SELECT  * FROM clienti WHERE IdCliente=?");
    $selectclienti->bind_param("i", $idcliente);
    $selectclienti->execute();
    $result = $selectclienti->get_result();

    echo "<table class=\"table\">
    <thead>
    <tr>
    <th scope=\"col\">IdCliente</th>
    <th scope=\"col\">CF</th>
    <th scope=\"col\">Cognome</th>
    <th scope=\"col\">Nome</th>
    <th scope=\"col\">Email</th>
    <th scope=\"col\">Telefono</th>
    <th scope=\"col\">Via</th>
    <th scope=\"col\">Numero</th>
    <th scope=\"col\">Citta'</th>
    <th scope=\"col\">CAP</th>
    </tr>
    </thead>
    <tbody>";

    while ($row = $result->fetch_assoc()) {
      echo "<tr>";
      // echo "<th scope=\"row\">";
      echo "<td>" . $row['IdCliente'] . "</td>";
      echo "<td>" . $row['CF'] . "</td>";
      echo "<td>" . $row['Cognome'] . "</td>";
      echo "<td>" . $row['Nome'] . "</td>";
      echo "<td>" . $row['Email'] . "</td>";
      echo "<td>" . $row['Telefono'] . "</td>";
      echo "<td>" . $row['IndVia'] . "</td>";
      echo "<td>" . $row['IndNumero'] . "</td>";
      echo "<td>" . $row['IndCitta'] . "</td>";
      echo "<td>" . $row['IndCap'] . "</td>";
      // echo "</th>";
      echo "</tr>";
     }

     echo "</tbody></table>";

      $selectclienti->close();

    $conn->close();

}
?>
