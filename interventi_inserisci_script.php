<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }
//
// print_r($_POST);

if (isset($_POST['clienteselect']) &&
    isset($_POST['dispositivoselect']) &&
    isset($_POST['deposito']))   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['clienteselect'];
    $iddispositivo = (int) $_POST['dispositivoselect'];
    $deposito = (double) $_POST['deposito'];
    $data = date("Y-m-d");

    function inserisciIntervento($conn, $idcliente, $iddispositivo, $deposito, $data){

      // require("db_connection.php");
      // require("use_db.php");

        $stmt = $conn->prepare("INSERT INTO depositi (Data, Importo)
                                  VALUES (?, ?)");


        $stmt->bind_param("sd", $data, $deposito);

        if ($stmt->execute() == TRUE) {

          $selectid = $conn->prepare("SELECT LAST_INSERT_ID() FROM depositi");
          $selectid->execute();
          $result = $selectid->get_result();

          if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            $iddeposito = $row['LAST_INSERT_ID()']; // restituisce id corrente


            $selectdispositivocliente = $conn->prepare("SELECT MAX(IdIntervento)
                                                        FROM interventi
                                                        WHERE Cliente=?
                                                        AND Dispositivo=?");
            $selectdispositivocliente->bind_param("ii", $idcliente, $iddispositivo);
            $selectdispositivocliente->execute();
            $result = $selectdispositivocliente->get_result();
            $row = $result->fetch_assoc();
            // //
            // print_r($row);

            if(isset($row['MAX(IdIntervento)'])){
              // echo "bella pe tutti esiste id dispositivo";
              //     echo "Max id is " . $row['MAX(IdDispositivo)'];
                  $idintervento = $row['MAX(IdIntervento)']+1;
            } else {
              // echo "odio il mondo infatti id dispositivo e' null";
              $idintervento = 1;
            }



            $selectdispositivocliente->close();



              $inserimentointervento = $conn->prepare("INSERT INTO interventi (Cliente, Dispositivo, IdIntervento, Deposito, DataRicezione)
                                        VALUES (?, ?, ?, ?, ?)");


              $inserimentointervento->bind_param("iiids", $idcliente, $iddispositivo, $idintervento, $iddeposito, $data);

              if ($inserimentointervento->execute() == TRUE) {

                echo "<script>
                alert('Intervento inserito correttamente');
                window.location.href='home.php';
                </script>";

              } else {
                echo $inserimentointervento->error;
              }


          } else {
                  echo $stmt->error;
        }

        $stmt->close();

      }
      $conn->close();

    } // chiusura function inserisciIntervento()

    $selectdata = $conn->prepare("SELECT
                                          	DataFine
                                          FROM
                                              interventi
                                          WHERE
                                              Cliente = ?
                                          AND Dispositivo = ?");

    $selectdata->bind_param("ii", $idcliente, $iddispositivo);
    $selectdata->execute();
    $result = $selectdata->get_result();
    if ($result->num_rows === 0) { // se non ci sono tuple con idcliente e iddispositivo passati come parametro,
                                  //aggiungo l'intervento
      // echo "non esiste id cliente e id dispositivo quindi inserisci intervento";
      inserisciIntervento($conn, $idcliente, $iddispositivo, $deposito, $data);
    } else { // altrimenti se le tuple esistono potrebbero avere in DataFine dei valori oppure null oppure entrambi ...
      // echo "le tuple esistono e potrebbero essere con valori o null";

      $datanotnull = true; // ... assumo temporaneamente che la DataFine di tutte le tuple selezionate esista e sia diversa da NULL
                          // ovvero è il caso in cui dovrei inserire un nuovo intervento, poiché quelli esistenti sono tutti terminati
                          // ovvero hanno tutti datafine diversa da NULL

      while ($row = $result->fetch_assoc()) { // ... controllo tutti i valori di DataFine per verificare quanto affermato sopra ...
           // print_r($row);
           // echo " valore di isset " . isset($row['DataFine']);
           if (!isset($row['DataFine'])) { // ... se un valore di DataFine non è settato ovvero é uguale a NULL
                                          // ovvero esiste un intervento già aperto dato quel cliente e quel dispositivo ...
             // echo "esiste una variabile null!";
             $datanotnull = false; // ... setto la var con valore false
           }

       }

       if ($datanotnull) { // se è stato verificato che tutte le tuple selezionate hanno una data diversa da NULL
                          // ovvero tutti gli interventi risultano chiusi, allora aggiungi intervento
         inserisciIntervento($conn, $idcliente, $iddispositivo, $deposito, $data);
       } else { // se invece è stato trovato un intervento con valore DataFine uguale a NULL
                // ovvero esiste un intervento che risulta ancora aperto,
                // allora segnaliamo che esiste già un intervento aperto per quel cliente e dispositivo
         // echo "attenzione esiste già un intervento aperto per cliente e dispositivo selezionato";
         echo "<script>
         alert('Intervento non inserito. Esiste già un intervento aperto per IdCliente: " . $idcliente . " e IdDispositivo: " . $iddispositivo . "');
         window.location.href='interventi.php';
         </script>";
       }

    }
    $selectdata->close();

}
?>
