<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

// echo "benvenuto in inserisci dispositivi";
//
// print_r($_POST);

if (isset($_POST['clienteselect']) &&
    isset($_POST['marca']) &&
    isset($_POST['modello']))   {

    require("db_connection.php");
    require("use_db.php");

    $idcliente = (int) $_POST['clienteselect'];
    $marca = $_POST['marca'];
    $modello = $_POST['modello'];

    if (isset($_POST['seriale'])) {
      $seriale = $_POST['seriale'];
    } else {
      $seriale = NULL;
    }



    $selectclienti = $conn->prepare("SELECT MAX(IdDispositivo) FROM dispositivi WHERE Cliente=?");
    $selectclienti->bind_param("i", $idcliente);
    $selectclienti->execute();
    $result = $selectclienti->get_result();
    $row = $result->fetch_assoc();
    // //
    // print_r($row);

    if(isset($row['MAX(IdDispositivo)'])){
      // echo "bella pe tutti esiste id dispositivo";
      //     echo "Max id is " . $row['MAX(IdDispositivo)'];
          $iddispositivo = $row['MAX(IdDispositivo)']+1;
    } else {
      // echo "odio il mondo infatti id dispositivo e' null";
      $iddispositivo = 1;
    }



    $selectclienti->close();



      $stmt = $conn->prepare("INSERT INTO dispositivi (Cliente, IdDispositivo, NumSeriale, Modello, Marca)
                                VALUES (?, ?, ?, ?, ?)");


      $stmt->bind_param("iisss", $idcliente, $iddispositivo, $seriale, $modello, $marca);

      if ($stmt->execute() == TRUE) {

        echo "<script>
        alert('Inserimento dispositivo completato');
        window.location.href='home.php';
        </script>";

      } else {
        echo $stmt->error;
      }


      $stmt->close();


    $conn->close();

}
?>
