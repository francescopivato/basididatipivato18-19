<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  // print_r($_POST);

  if (isset($_POST['idC']) &&
      isset($_POST['idD']) &&
      isset($_POST['idI'])) {



    require("db_connection.php");
    require("use_db.php");

    $idCliente = (int)($_POST['idC']);
    $idDispositivo = (int)($_POST['idD']);
    $idIntervento = (int)($_POST['idI']);

    $selectutilizzi = $conn->prepare("SELECT
                                          	C.Nome AS Categoria,
                                            E.Nome AS Elemento,
                                          	U.Data,
                                            U.Quantita,
                                            U.CostoTotale
                                        FROM
                                            utilizzi U
                                        INNER JOIN elementi E ON
                                            U.Elemento = E.IdElemento AND U.Categoria = E.Categoria
                                        INNER JOIN cat_elementi C ON
                                            U.Categoria = C.IdCat
                                        WHERE
                                            Cliente = ? AND Dispositivo = ? AND Intervento = ?");

    $selectutilizzi->bind_param("iii", $idCliente, $idDispositivo, $idIntervento);
    $selectutilizzi->execute();
    $result = $selectutilizzi->get_result();

    echo "<table class=\"table\">
    <thead>
    <tr>
    <th scope=\"col\">Categoria</th>
    <th scope=\"col\">Elemento</th>
    <th scope=\"col\">Data Utilizzo</th>
    <th scope=\"col\">Quantità</th>
    <th scope=\"col\">Costo Totale</th>
    </tr>
    </thead>
    <tbody>";

    while ($row = $result->fetch_assoc()) {

      echo "<tr>";
      // echo "<th scope=\"row\">";
      echo "<td>" . $row['Categoria'] . "</td>";
      echo "<td>" . $row['Elemento'] . "</td>";
      echo "<td>" . $row['Data'] . "</td>";
      echo "<td>" . $row['Quantita'] . "</td>";
      echo "<td>" . $row['CostoTotale'] . "</td>";
      // echo "</th>";
      echo "</tr>";
     }

     echo "</tbody></table>";

    $selectutilizzi->close();

  }

?>
