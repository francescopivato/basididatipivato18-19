<?php
  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  require("db_connection.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Acquisti</title>

    <script type="text/javascript">

      function mostraInserisci(){
        $("#modifica").hide();
        $("#elimina").hide();
        $("#visualizza").hide();
        $("#inserisci").toggle();
      }

      function mostraModifica(){
        $("#inserisci").hide();
        $("#elimina").hide();
        $("#visualizza").hide();
        $("#modifica").toggle();
      }

      function mostraElimina(){
        $("#inserisci").hide();
        $("#modifica").hide();
        $("#visualizza").hide();
        $("#elimina").toggle();
      }

      function mostraVisualizza(){
        $("#inserisci").hide();
        $("#modifica").hide();
        $("#elimina").hide();
        $("#visualizza").toggle();
      }

      function getElementi(idCat){ // per visualizzare elementi di una categoria
        // console.log("id cliente lato client is " + idCliente);

        $.post("acquisti_select_script.php", {
          id: idCat
        },
        function(data, status){
          // alert("Data: " + data + "\nStatus: " + status);
          $("#elementoselect").children().remove();
          $("#elementoselect").append(data);
        });

      }

      function getTecniciVisualizza(idTecnico){ // per visualizzazione dispositivi
        console.log("id tecnico lato client is " + idTecnico);

        $.post("acquisti_visualizza_script.php", {
          id: idTecnico
        },
        function(data, status){
          // alert("Data: " + data + "\nStatus: " + status);
          $("#txtAcquisti").children().remove();
          $("#txtAcquisti").append(data);
        });

      }

    </script>

</head>

<body>
    <div class="container-fluid">
      <div class="row title">
        <div class="col-sm-12">
          <header class="home-header">
            <h1 class="login-h1">REPAIR-IT</h1>
          </header>
        </div>
      </div>
      <div class="row nav">
        <div class="offset-sm-1 col-sm-10">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="home.php">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <a class="nav-item nav-link active" href="home.php">Home</a>
                <a class="nav-item nav-link" href="clienti.php">Clienti</a>
                <a class="nav-item nav-link" href="dispositivi.php">Dispositivi</a>
                <a class="nav-item nav-link" href="interventi.php">Interventi</a>
                <a class="nav-item nav-link" href="operazioni.php">Operazioni</a>
                <a class="nav-item nav-link" href="pagamenti.php">Pagamenti</a>
                <a class="nav-item nav-link" href="tecnici.php">Tecnici</a>
                <a class="nav-item nav-link" href="#">Acquisti</a>
                <a class="nav-item nav-link" href="categorie.php">Categorie</a>
                <a class="nav-item nav-link" href="elementi.php">Elementi</a>
                <a class="nav-item nav-link" href="utilizzi.php">Utilizzi</a>
                <a class="nav-item nav-link" href="logout.php">Logout</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
      <div class="row main">
        <div class="col-sm-12">
          <main>
            <div class="row buttons">
              <div class="offset-sm-2 col-sm-8">
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraInserisci()">Inserisci acquisto</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraModifica()">Modifica acquisto (X)</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraElimina()">Elimina acquisto (X)</button>
                  <button type="button" class="btn btn-outline-light btn-block mb-3 mt-3" onclick="mostraVisualizza()">Visualizza gli acquisti dei tecnici</button>
              </div>
            </div>
            <div class="row actions">
              <div class="offset-sm-1 col-sm-10">
                <div class="inserisci initiallyhidden" id="inserisci">
                  <form action="acquisti_inserisci_script.php" method="post">
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona la categoria: </legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="categoriaselect" class="col-sm-2 col-form-label sr-only">Categoria: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="categoriaselect" placeholder="Categoria" size="8" onchange="getElementi(this.value)" required >
                            <?php

                              require("db_connection.php");
                              require("use_db.php");

                              $selectcat = $conn->prepare("SELECT IdCat, Nome FROM cat_elementi");
                              $selectcat->execute();
                              $selectcat->bind_result($id, $nome);

                              while ($selectcat->fetch()) {
                                echo "<option value=\"" . $id . "\">" . $nome . "</option>";
                              }

                              $selectcat->close();

                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona l'elemento</legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="elementoselect" class="col-sm-2 col-form-label sr-only">Elemento: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="elementoselect" id="elementoselect" placeholder="Elemento" size="8" required >
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                        <div class="row legend">
                          <div class="offset-sm-2 col-sm-8">
                            <legend>Dati Acquisto</legend>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="quantita" class="col-sm-2 col-form-label sr-only">Quantità: </label>
                          <div class="offset-sm-2 col-sm-8">
                            <input type="number" id="quantita" name="quantita" class="form-control" placeholder="Quantità" min="1" required>
                          </div>
                        </div>
                      </fieldset>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-8">
                        <input class="btn btn-lg btn-secondary" type="submit" name="inserisci" id="inserisci" value="Inserisci">
                        <input class="btn btn-lg btn-secondary" type="reset" id="reset" value="Reset">
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modifica initiallyhidden" id="modifica">

                </div>
                <div class="elimina initiallyhidden" id="elimina">

                </div>
                <div class="visualizza initiallyhidden" id="visualizza">
                  <form action="#" method="post">
                    <fieldset>
                      <div class="row legend">
                        <div class="offset-sm-2 col-sm-8">
                          <legend>Seleziona un tecnico: </legend>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="tecnicoshow" class="col-sm-2 col-form-label sr-only">Tecnico: </label>
                        <div class="offset-sm-2 col-sm-8">
                          <select class="form-control" name="tecnicoshow" placeholder="Tecnico" size="8" onchange="getTecniciVisualizza(this.value)" required >
                            <?php

                              require("db_connection.php");
                              require("use_db.php");

                              $selecttecnici = $conn->prepare("SELECT IdTecnico, Cognome, Nome, Email FROM tecnici");
                              $selecttecnici->execute();
                              $selecttecnici->bind_result($id, $cognome, $nome, $email);

                              while ($selecttecnici->fetch()) {
                                echo "<option value=\"" . $id . "\">" . $id . " - " . $cognome . " " . $nome . " " . $email . "</option>";
                              }

                              $selecttecnici->close();

                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                  <div id="txtAcquisti" class="col-sm-12"></div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
</body>
</html>
