<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

// echo "benvenuto in inserisci elementi";
//
// print_r($_POST);

if (isset($_POST['categoriaselect']) &&
    isset($_POST['nome']) &&
    isset($_POST['costo']) &&
    isset($_POST['scorte']))   {

    require("db_connection.php");
    require("use_db.php");

    $idcat = (int) $_POST['categoriaselect'];
    $nome = $_POST['nome'];
    $costo = (float) $_POST['costo'];
    $scorte = (int) $_POST['scorte'];


    $selectelementi = $conn->prepare("SELECT MAX(IdElemento) FROM elementi WHERE Categoria=?");
    $selectelementi->bind_param("i", $idcat);
    $selectelementi->execute();
    $result = $selectelementi->get_result();
    $row = $result->fetch_assoc();
    // //
    // print_r($row);

    if(isset($row['MAX(IdElemento)'])){
      // echo "bella pe tutti esiste id dispositivo";
      //     echo "Max id is " . $row['MAX(IdDispositivo)'];
          $idelemento = $row['MAX(IdElemento)']+1;
    } else {
      // echo "odio il mondo infatti id dispositivo e' null";
      $idelemento = 1;
    }



    $selectelementi->close();



      $stmt = $conn->prepare("INSERT INTO elementi (Categoria, IdElemento, Nome, Costo, Scorte)
                                VALUES (?, ?, ?, ?, ?)");


      $stmt->bind_param("iisdi", $idcat, $idelemento, $nome, $costo, $scorte);

      if ($stmt->execute() == TRUE) {

        echo "<script>
        alert('Inserimento elemento completato');
        window.location.href='home.php';
        </script>";

      } else {
        echo $stmt->error;
      }


      $stmt->close();

    $conn->close();

}
?>
